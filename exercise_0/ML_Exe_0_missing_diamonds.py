import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import os.path as path
from os.path import dirname as up
import random
import seaborn as sns
import streamlit as st
#Libraries

with st.echo():
    np.random.seed(28938547)
    #create a boolean Numpy array of the same size diamonds DataFrame
    #around percentage_missing of the elments are True
    percentage_missing = 0.05
    nan_mat = np.random.random(df.shape) < percentage_missing
    
    #Pandas’ function mask to each element in the dataframe. 
    #The mask function will use the element in the dataframe if the condition is False and change it to NaN if it is True.
    #pd.mask(cond, other=nan)
    df_NAN = df.mask(nan_mat)
    df_NAN["price"] = df["price"] #Avoids the labelled attribute to have missing

    total_missing_values = df_NAN.isnull().sum()
    percentage_missing_values = total_missing_values/df_NAN.shape[0]

st.markdown(f"The dataset does not have missing values. However, to make it more realistic we generate around of {percentage_missing*100} % of missing values over the columns.")
st.markdown("Missing per column: ")
st.write(total_missing_values)
st.markdown("Percentage of missing values: ")
st.write(percentage_missing_values)
st.write("Now the dataset contains missing values: ")
st.write(df_NAN.head(50))
