import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import os.path as path
from os.path import dirname as up
import random
import seaborn as sns
import streamlit as st

def alles_good_papi():
    return("Alles good papi")


def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())


def generate_missing(dataset):
    percentage = 0.2 # percentage of values to replace in one column or randomly across all attributes
    feature = None # column name to replace values only for a specific column e.g.: 'LB' or None to replaces values across all feature attributes
    random_seed = 9103 # initialization parameter of the random number generator
    random_seed_iteration = 1
    file_name = two_up + '\data\cgt_missing_'+percentage+'.csv' # file name of the output file
    
    random.seed(random_seed)
    #dataset = read()

    columne_names = list(dataset.columns.values)
    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'Width',
                'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean', 'Median', 'Variance', 'Tendency']
    number_of_features = len(features)
    number_of_rows = len(dataset.index)
    num_rxf = number_of_rows * number_of_features

    st.write("Number of columns: %d" % len(columne_names))
    st.write("Number of (real) features: %d" % number_of_features)
    st.write("Number of rows: %d" % number_of_rows)
    st.write("Number of feature values: %d" % num_rxf)

    if feature is None:
        number_of_missing_values = int(num_rxf * percentage)
        st.write("Number of feature values to replace with NaN: %d" % number_of_missing_values)
        for x in range(number_of_missing_values): 
            random_seed = random_seed + random_seed_iteration
            random.seed(random_seed)
            random_column = random.choice(features) # only replace values with NaN for real features
            dataset.loc[dataset.sample(n=1, random_state=random_seed).index, random_column] = np.NaN
        write(dataset, file_name)
    else:
        dataset_percentage = dataset.sample(frac=percentage, random_state=random_seed)
        data_rest = dataset.drop(dataset_percentage.index)
        number_of_missing_values_for_column = len(dataset_percentage.index)
        st.write("Number of feature value for column '%s' to replace with NaN: %d" % (feature, number_of_missing_values_for_column))
        dataset_percentage[feature] = np.NaN
        result = pd.concat([data_rest, dataset_percentage])
        result.sort_index(inplace=True) # sort by index
        write(result, file_name)
        
    return(alles_good_papi())


st.markdown("# Machine Learning 2020S")
st.markdown("# Exercise 0 - Group 32")
st.markdown("## Importing the datasets - Cardiotocography")
st.markdown('The data set looks like:')


with st.echo():
    # READ FROM DATA DIR
    two_up = up(up(__file__))
    df_p = pd.read_csv(two_up+'\data\cardio_0.csv', sep = ';', encoding='utf-8')
    cardio = df_p
    desc = cardio.describe()

#st.write(two_up)
#st.write(cardio)
st.write(cardio.shape)
st.write(desc)
st.write(desc.shape)

st.markdown("## Generating Missing Values")
st.markdown('This dataset does not contain any missing values. Therefore, the strategy is to genetate them randomly adding NaNs all across the features:')

with st.echo():
    # MISSING VALUES ON CARDIO
    dataset = cardio
    percentage = 0.2 # percentage of values to replace in one column or randomly across all attributes
    feature = None # column name to replace values only for a specific column e.g.: 'LB' or None to replaces values across all feature attributes
    random_seed = 9103 # initialization parameter of the random number generator
    random_seed_iteration = 1
    file_name = two_up + '\data\cgt_missing_'+str(percentage)+'.csv' # file name of the output file
    
    random.seed(random_seed)
    #dataset = read()

    columne_names = list(dataset.columns.values)
    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'Width',
                'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean', 'Median', 'Variance', 'Tendency']
    number_of_features = len(features)
    number_of_rows = len(dataset.index)
    num_rxf = number_of_rows * number_of_features

    st.write("Number of columns: %d" % len(columne_names))
    st.write("Number of (real) features: %d" % number_of_features)
    st.write("Number of rows: %d" % number_of_rows)
    st.write("Number of feature values: %d" % num_rxf)
    number_of_missing_values = int(num_rxf * percentage)
    st.write("Number of feature values to replace with NaN: %d" % number_of_missing_values)

    for x in range(number_of_missing_values): 
        random_seed = random_seed + random_seed_iteration
        random.seed(random_seed)
        random_column = random.choice(features) # only replace values with NaN for real features
        dataset.loc[dataset.sample(n=1, random_state=random_seed).index, random_column] = np.NaN
    
    write(dataset, file_name)

    cardio_m = dataset


    desc = cardio_m.describe()

st.markdown('The dataset with missing values looks like:')

st.write(cardio_m)
st.write(cardio_m.shape)
st.write(desc)
st.write(desc.shape)

st.markdown("## Characterizing the datasets")
st.markdown('Here we generate some plots:')


with st.echo():

    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'Width', 
                'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean', 'Median', 'Variance', 'Tendency']
    
    for f in features:
        
        bplot=sns.boxplot(y=cardio[f], x=cardio['CLASS'], 
                        #data=cardio_plot,
                        width=0.5,
                        palette="colorblind")
        
        #bplot.set_xticklabels(bplot.get_xticklabels(), rotation=-45)

        
        # add swarmplot
        #bplot=sns.swarmplot(y=cardio[f], x=cardio['CLASS'], 
                        #data=cardio_plot,
                        #color=".2",
                        #alpha=0.75)

        st.pyplot()



st.write(alles_good_papi())
st.balloons()