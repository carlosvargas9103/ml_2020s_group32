import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import os.path as path
from os.path import dirname as up
import random
import seaborn as sns
import streamlit as st
#CARLOS LIBRARIES

'''
import scipy
import seaborn as sns
import sklearn as sk
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import mutual_info_classif
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn import tree
from sklearn.tree import _tree
from sklearn.tree import DecisionTreeClassifier as DTC

from sklearn.model_selection import cross_val_score
from sklearn import linear_model
from sklearn import neighbors
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold
import category_encoders as ec
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler

#from sklearn.decomposition import PCA


#@st.cache
#np.random.seed(9103)


st.markdown("## Identifying emerging countries from poverty")
with st.echo():
    # Poor countries
    p_poor = poor[poor['poverty'] == 1]
    c_poor = p_poor.LOCATION.unique() # poor countries

    # No Poor countries
    no_poor = poor[(poor['poverty'] == 0) & (poor['TIME'] == 2015)]
    #no_poor = no_poor[poor['TIME'] == 2015]
    c_no_poor = no_poor.LOCATION.unique() # no poor countries 2015
    e_countries = set(c_no_poor).intersection(c_poor) # Identifying emerging countries
    #e_countries # Emerging
    poor['emerging'] = poor['LOCATION'].isin(e_countries)
    e_poor = poor[poor['LOCATION'].isin(e_countries)]
    #st.write(poor)
    st.write(thresholds(df_poor=poor))
    st.write(e_poor)
    st.write(len(e_countries))
    st.write(len(poor.LOCATION.unique()))

def years_emerged(e_poor):
    years = {}
    years_total = {}
    e_countries = list()
    df_e_poor = e_poor
    for y in df_e_poor.TIME.unique():
        df_y = df_e_poor[df_e_poor['TIME'] == y]
        countries = list()
        #countries_total = list()
        for c in df_y.LOCATION.unique():
            df_c = df_y[df_y['LOCATION'] == c]
            if ((df_c.iloc[0,-2] == 0) & (c not in e_countries)):
                #st.write(df_c.iloc[0,-2])
                e_countries.append(c)
                countries.append(c)
            #break
        #st.write(y,countries)
        y = int(y)
        years[y] = countries
        years_total[y] = len(countries)
    # PLOT
    #sns.distplot(years, kde=False)
    plt.bar(years_total.keys(), years_total.values(), color='g')
    st.pyplot()

    return(years)

def linearModel(df_e_poor, alpha=0.1, fit_intercept=True, normalize=True, solver='auto', max_iter=1000, tol=0.0001):
    e_poor = df_e_poor
    # Split dataset to train
    X = e_poor.iloc[:,2:-2] # All the columns less the last one
    #st.write(X.columns)
    y = e_poor.iloc[:,-2:-1] # Just the last column

    lm = linear_model.RidgeClassifier(alpha=alpha, fit_intercept=fit_intercept, normalize=normalize, max_iter=max_iter, tol=0.001, solver='auto', random_state=30)
    lm.fit(X,y)
    coefficients = pd.concat([pd.DataFrame(X.columns),pd.DataFrame(np.transpose(lm.coef_))], axis = 1)
    st.write(coefficients)
    return(lm)


st.markdown("## Building a Model")
st.markdown("[Feature Selection Techniques in Machine Learning with Python] ('https://towardsdatascience.com/feature-selection-techniques-in-machine-learning-with-python-f24e7da3f36e')")
with st.echo():
    st.write("Best ")
    st.write("f_classif")
    st.write(e_poor_selectKBest(e_poor,score_f=f_classif))
    st.write("chi2")
    st.write(e_poor_selectKBest(e_poor,score_f=chi2))
    st.write("mutual_info_classif")
    st.write(e_poor_selectKBest(e_poor,score_f=mutual_info_classif))
    st.write("feature_importance")
    e_poor_feature_importance(e_poor)
    st.write("Correlation Matrix")
    plot_correlation_matrix(e_poor)
    st.write("Linear Model")
    lm = linearModel(df_e_poor = e_poor)
    #st.write(e_poor.columns)
    #st.write(lm)
    #st.wrtie(lm.get_params)
    #st.write(lm.coef_)


st.markdown("## Whether the countries emerged")
with st.echo():
    #years_emerged(e_poor)
    st.write(years_emerged(e_poor))
    

def just_demographic(df_poor):
    feature_descriptions = pd.read_csv("data/feature_descriptions.csv")

    #FEATURES WITH LESS THAN 50% MISSING VALUES
    features = feature_descriptions.where(feature_descriptions['na_percent']<=50.0).dropna(0)
    
    #ONLY DEMOGRAFIC FEATURES!
    cols = features['Unnamed: 0'].tolist()
    cols = cols[0:7]+ cols[13:18] + [cols[25]]
    df_poor = df_poor[cols]
    return (df_poor)

def interpolation_df_poor(df_poor):
    columns = df_poor.columns
    countries = df_poor['LOCATION'].unique()
    for c in countries:
        df_c = df_poor[df_poor['LOCATION']==c]
        df_c = df_c.interpolate(method ='linear', 
                                    limit_direction ='forward',
                                    axis = 0)
        df_c = df_c.interpolate(method ='linear', 
                                    limit_direction ='backward',
                                    axis = 0)
        df_c.fillna(poor.drop(labels='LOCATION', axis=1).mean(), inplace =True)
        df_poor[df_poor['LOCATION']==c] = df_c
    return(df_poor)

def thresholds(df_poor):
    poor = df_poor
    thresholds = {}
    columns = poor.columns[2:-2]
    #st.write(columns)
    X = poor.iloc[:,2:-2]
    #st.write(len(X))
    #st.write(columns)
    y = poor.iloc[:,-2:-1]
    #st.write(len(y))
    #st.write(y)
    for c in columns:
        #st.write(c)
        #st.write(X[c])
        X_ = np.array(X[c]).reshape(-1, 1)#X.iloc[:,c]
        clf_tree = DTC(criterion="gini",
                        max_depth=1, 
                        splitter="best")
        clf_tree.fit(X_,y)
        #thresholds[c] = clf_tree.tree_.threshold[0]
        threshold = clf_tree.tree_.threshold[0]
        #st.write(X_.mean())
        #st.write(threshold)
        #break        
        thresholds[c] = threshold    
    return(thresholds)


def e_poor_selectKBest(df_e_poor, score_f = f_classif, k='all'):
    e_poor = df_e_poor
    # Split dataset to train
    X = e_poor.iloc[:,2:-2] # All the columns less the last one
    y = e_poor.iloc[:,-2:-1] # Just the last column
    dfcolumns = pd.DataFrame(X.columns)

    scaler = MinMaxScaler()
    scaler.fit(X)
    X = scaler.transform(X)
    # Create the feature selector
    #perhaps a switch
    bestFeatures = SelectKBest(score_func=score_f, k=k)
    fit = bestFeatures.fit(X,y)
    dfscores = pd.DataFrame(fit.scores_)

    # Create a data frame to see the impact of the features
    featureScores = pd.concat([dfcolumns,dfscores],axis=1)
    featureScores.columns = ['Features','Scores']
    featureScores.sort_values(by='Scores', ascending=False, inplace=True)
    scores = featureScores.Scores
    rel_scores = np.array(scores)/np.abs(np.array(scores)).sum()
    rel_scores.reshape(-1, 1)
    #st.write(rel_scores.shape)

    rel_scores = pd.DataFrame(rel_scores, dtype=float, columns=['Relative'])

    df_scores = pd.concat([featureScores,rel_scores], axis=1)
    #st.write(df_scores)
    #st.write(scores)
    #st.write(rel_scores)
    return(df_scores)

def e_poor_feature_importance(df_e_poor):
    e_poor = df_e_poor
    # Split dataset to train
    X = e_poor.iloc[:,2:-2] # All the columns less the last one
    y = e_poor.iloc[:,-2:-1] # Just the last column
    model = ExtraTreesClassifier()
    model.fit(X,y)
    print(model.feature_importances_)
    feat_importances = pd.Series(model.feature_importances_, index=X.columns)
    #st.write(feat_importances)

    #columns = pd.DataFrame(feat_importances.index)
    #st.write(columns)
    scores = pd.DataFrame(feat_importances, dtype=float)
    featureScores = scores.reset_index()
    featureScores.columns = ['Features','Scores']
    df_scores0 = featureScores.sort_values(by='Scores', ascending=False)
    df_scores0.reset_index(drop=True, inplace=True)
    #st.write(featureScores)
    scores = df_scores0.Scores
    rel_scores = np.array(scores)/np.abs(np.array(scores)).sum()
    rel_scores.reshape(-1, 1)
    #st.write(rel_scores.shape)
    rel_scores = pd.DataFrame(rel_scores, dtype=float, columns=['Relative'])
    df_scores = pd.concat([df_scores0,rel_scores], axis=1)
    st.write(df_scores)
    # Plot it in a barchart
    feat_importances.nlargest(20).plot(kind='barh', figsize = (13, 6), fontsize=12)
    st.pyplot()
    return("All fertig pa pitura")


def plot_correlation_matrix(df_e_poor, n=20):
    data = e_poor.iloc[:,2:-2] # All features
    # Split dataset to train
    columns = data.columns
    plt.clf()
    correlation = data.corr()
    #columns = correlation.nlargest(n, 'poverty').index
    #st.write('OK')
    #st.write(columns)
    #st.write(data[columns].values)

    correlation_map = np.corrcoef(data[columns].values.T)
    sns.set(font_scale=1, rc={'figure.figsize':(30,30)})
    heatmap = sns.heatmap(correlation_map, cbar=True, annot=False, 
                            square=True, fmt='.2f', yticklabels=columns.values, 
                            xticklabels=columns.values)
    heatmap.set_xticklabels(heatmap.get_xticklabels(), rotation = 90, fontsize = 16)
    heatmap.set_yticklabels(heatmap.get_yticklabels(), rotation = 0, fontsize = 16)
    st.pyplot()
    return("All fertig pa pitura")

# correlation map cardio
plt.clf()
correlation = dataset.corr()
columns = correlation.nlargest(22, 'CLASS').index
correlation_map = np.corrcoef(dataset[columns].values.T)
sns.set(font_scale=1.0)
heatmap = sns.heatmap(correlation_map, cbar=True, annot=True, square=True, fmt='.2f', yticklabels=columns.values, xticklabels=columns.values)
plt.show()

'''

def alles_good_papi():
    return("Alles good papii")


def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())


def generate_missing(dataset):
    percentage = 0.2 # percentage of values to replace in one column or randomly across all attributes
    feature = None # column name to replace values only for a specific column e.g.: 'LB' or None to replaces values across all feature attributes
    random_seed = 9103 # initialization parameter of the random number generator
    random_seed_iteration = 1
    file_name = two_up + '\data\cgt_missing_'+percentage+'.csv' # file name of the output file
    
    random.seed(random_seed)
    #dataset = read()

    columne_names = list(dataset.columns.values)
    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'Width',
                'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean', 'Median', 'Variance', 'Tendency']
    number_of_features = len(features)
    number_of_rows = len(dataset.index)
    num_rxf = number_of_rows * number_of_features

    st.write("Number of columns: %d" % len(columne_names))
    st.write("Number of (real) features: %d" % number_of_features)
    st.write("Number of rows: %d" % number_of_rows)
    st.write("Number of feature values: %d" % num_rxf)

    if feature is None:
        number_of_missing_values = int(num_rxf * percentage)
        st.write("Number of feature values to replace with NaN: %d" % number_of_missing_values)
        for x in range(number_of_missing_values): 
            random_seed = random_seed + random_seed_iteration
            random.seed(random_seed)
            random_column = random.choice(features) # only replace values with NaN for real features
            dataset.loc[dataset.sample(n=1, random_state=random_seed).index, random_column] = np.NaN
        write(dataset, file_name)
    else:
        dataset_percentage = dataset.sample(frac=percentage, random_state=random_seed)
        data_rest = dataset.drop(dataset_percentage.index)
        number_of_missing_values_for_column = len(dataset_percentage.index)
        st.write("Number of feature value for column '%s' to replace with NaN: %d" % (feature, number_of_missing_values_for_column))
        dataset_percentage[feature] = np.NaN
        result = pd.concat([data_rest, dataset_percentage])
        result.sort_index(inplace=True) # sort by index
        write(result, file_name)
        
    return(alles_good_papi())



st.markdown("# Machine Learning 2020S - Exercise 0")
st.markdown("## Importing the datasets - Cardiotocography")
st.markdown('The data set looks like:')


with st.echo():
    # READ FROM DATA DIR
    two_up = up(up(__file__))
    df_p = pd.read_csv(two_up+'\data\cardio_0.csv', sep = ';', encoding='utf-8')
    cardio = df_p
    desc = cardio.describe()
    
st.write(cardio)
st.write(cardio.shape)
st.write(desc)
st.write(desc.shape)

st.markdown("## Generating Missing Values")
st.markdown('This dataset does not contain any missing values. Therefore, the strategy is to genetate them randomly adding NaNs all across the features:')

with st.echo():
    # MISSING VALUES ON CARDIO
    dataset = cardio
    percentage = 0.2 # percentage of values to replace in one column or randomly across all attributes
    feature = None # column name to replace values only for a specific column e.g.: 'LB' or None to replaces values across all feature attributes
    random_seed = 9103 # initialization parameter of the random number generator
    random_seed_iteration = 1
    file_name = two_up + '\data\cgt_missing_'+str(percentage)+'.csv' # file name of the output file
    
    random.seed(random_seed)
    #dataset = read()

    columne_names = list(dataset.columns.values)
    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'Width',
                'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean', 'Median', 'Variance', 'Tendency']
    number_of_features = len(features)
    number_of_rows = len(dataset.index)
    num_rxf = number_of_rows * number_of_features

    st.write("Number of columns: %d" % len(columne_names))
    st.write("Number of (real) features: %d" % number_of_features)
    st.write("Number of rows: %d" % number_of_rows)
    st.write("Number of feature values: %d" % num_rxf)
    number_of_missing_values = int(num_rxf * percentage)
    st.write("Number of feature values to replace with NaN: %d" % number_of_missing_values)

    for x in range(number_of_missing_values): 
        random_seed = random_seed + random_seed_iteration
        random.seed(random_seed)
        random_column = random.choice(features) # only replace values with NaN for real features
        dataset.loc[dataset.sample(n=1, random_state=random_seed).index, random_column] = np.NaN
    
    write(dataset, file_name)

    cardio_m = dataset


    desc = cardio_m.describe()

st.markdown('The dataset with missing values looks like:')

st.write(cardio_m)
st.write(cardio_m.shape)
st.write(desc)
st.write(desc.shape)

st.markdown("## Characterizing the datasets")
st.markdown('Here we generate some plots:')

cardio_plot = cardio[features]
st.write(cardio_plot.columns)

with st.echo():

    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'Width', 
                'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean', 'Median', 'Variance', 'Tendency']
    
    for f in features:
        
        #st.write(cardio[f])
        #cardio_bplot = cardio[f]#.to_frame()
        #st.write(cardio_bplot.head())

        #data_temperature.boxplot()
        #data_weather_cleaned[5:11].boxplot()

        # plot boxplot with seaborn
        
        bplot=sns.boxplot(y=cardio[f], x=cardio['CLASS'], 
                        #data=cardio_plot,
                        width=0.5,
                        palette="colorblind")
        
        #bplot.set_xticklabels(bplot.get_xticklabels(), rotation=-45)

        '''
        # add swarmplot
        bplot=sns.swarmplot(y=cardio[f], x=cardio['CLASS'], 
                        #data=cardio_plot,
                        color=".2",
                        alpha=0.75)

        '''
        st.pyplot()



st.write(alles_good_papi())
st.balloons()