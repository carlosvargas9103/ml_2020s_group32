import pandas as pd
from os.path import dirname as up
import streamlit as st


with st.echo():
    # READ FROM DATA DIR
    two_up = up(up(__file__))
    df = pd.read_csv(two_up+'\data\\biastemperatureMeanTmax_0_clean.csv', sep = ';', encoding='utf-8')
    st.write(df.columns.tolist())

st.balloons()