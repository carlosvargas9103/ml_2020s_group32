import streamlit as st
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import os.path as path
from os.path import dirname as up
from unipath import Path
import random
import seaborn as sns
import sklearn as sk
import plotly.express as px
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler


#from sklearn.metrics import confusion_matrix
import time
from joblib import Parallel, delayed
import multiprocessing

start_0 = time.time()
num_cores = multiprocessing.cpu_count()

def alles_good_papi():
    return("Alles good papi")

#FUNCTION QUE GUARDA DATASETS
def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())

#FUNCTION CREATE_LMS
def create_LMS(models = []):
    normalize = [False, True]
    bias = [False, True]
    interaction = [False, True]
    intercept = [True, False]
    degree = [ 1, 2]

    # CREATE MODELS
    m_id = 1
    for n in normalize:
        for b in bias:
            for interact in interaction:
                for inter in intercept:
                    for d in degree:
                        models.append([m_id,n,b,interact,inter,d])
                        m_id = m_id+1
    return models

    

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models(i):
    path = up(__file__)
    file_name = path+'\\folds\housedata_clean_without_non-correlated_columns_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="str")
    file_name = path+'\\folds\housedata_clean_without_non-correlated_columns_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="str")
    X_train = df_train.drop(['price'], axis=1)
    y_train = df_train['price']
    X_test = df_test.drop(['price'], axis=1)
    y_test = df_test['price']

    results = pd.DataFrame()
    models = create_LMS([])    
    for m in models:
        start = time.time()
        result_row = {}
        m_id,n,b,interact,inter,d = m[:]
        result_row["model"] = m_id
        result_row["normalize"] = n
        result_row["bias"] = b
        result_row["interaction"] = interact
        result_row["intercept"] = inter
        result_row["degree"] = d
        poly = PolynomialFeatures(include_bias=b, interaction_only=interact, degree=d)
        lrm = LinearRegression(normalize=n, fit_intercept=inter)
        X_train_t = poly.fit_transform(X_train)
        X_test_t = poly.fit_transform(X_test)
        lrm.fit(X_train_t, y_train)
        result_row["score"] = round(lrm.score(X_test_t, y_test),6)
        result_row["time"] = round(time.time() - start,6)
        results = results.append(result_row, ignore_index = True)
    return results

def aggregate_results(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['normalize'] = results.groupby('model')['normalize'].max()
    results_folds['bias'] = results.groupby('model')['bias'].max()
    results_folds['interaction'] = results.groupby('model')['interaction'].max()
    results_folds['intercept'] = results.groupby('model')['intercept'].max()
    results_folds['degree'] = results.groupby('model')['degree'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()
    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by='model_id', ascending=True, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    return results_folds

### HERE WE JUST SHOW THE IMPLEMENTATION

st.markdown("# Machine Learning 2020S")
st.markdown("# Exercise 1 - Regression - Group 32")
st.markdown("## Linear Model - housedata_clean_nocorr")
st.markdown('The data set looks like:')

with st.echo():
    # READ FROM DATA DIR
    two_up = up(up(__file__))
    df_p = pd.read_csv(two_up+'\data\housedata_0_clean_non_corr_col.csv', sep = ';', encoding='utf-8')
    housedata = df_p
    desc = housedata.describe()

#st.write(two_up)
st.write(housedata)
st.write(housedata.shape)
st.write(desc)
st.write(desc.shape)

st.markdown("## Preprocessing the dataset for the Regression model")
st.markdown('First selecting some of the columns')


with st.echo():
    features = [
    "price",
  "bedrooms",
  "bathrooms",
  "sqft_living",
  "sqft_lot",
  "floors",
  "waterfront",
  "sqft_above",
  "sqft_basement",
  "yr_built",
  "yr_renovated",
  "lat",
  "sqft_living15",
  "sqft_lot15",
  "grade_1",
  "grade_10",
  "grade_11",
  "grade_12",
  "grade_13",
  "grade_3",
  "grade_4",
  "grade_5",
  "grade_6",
  "grade_7",
  "grade_8",
  "grade_9",
  "view_0",
  "view_1",
  "view_2",
  "view_3",
  "view_4"
]

df = housedata.loc[:,features]

st.markdown("## Split the dataset into 10-Folds for Cross-Validation")
st.markdown('The resulting datasets are store in the directory /folds')

with st.echo():
    current = Path(os.getcwd())
    cv = KFold(n_splits=10, random_state=9103, shuffle=True)
    #cv = GroupKFold(n_splits=10) # This is for not overlapping groups
    fold = 1
    for train_index, test_index in cv.split(df):
        df.iloc[train_index,:].to_csv(current+'\\folds\housedata_clean_without_non-correlated_columns_train_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        df.iloc[test_index,:].to_csv(current+'\\folds\housedata_clean_without_non-correlated_columns_test_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        fold = fold+1

st.markdown('One dataset for training looks like:')
st.write(df.iloc[train_index,:],df.iloc[train_index,:].shape)
st.markdown('One dataset for testing looks like:')
st.write(df.iloc[test_index,:],df.iloc[test_index,:].shape)

st.markdown('## Fitting the models!')
st.markdown('Here we fit the models parallelizing the cross-validation')

with st.echo():

    results = pd.DataFrame()
    arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models)(i)  for i in range (1,11))
    for a in arr_results:
        results = results.append(a, ignore_index = True)
    results = aggregate_results(results)    
    
    # EXPORT THE FILE
    current = up(__file__)
    file_name = current + '\\target\housedata\housedata_clean_without_non-correlated_columns_results_LM_folds.csv' # file name of the output file
    write(results,file_name)

st.markdown('Here we can see the results of the computation of the models with different parameters')
st.write(results,results.shape)

st.markdown("## Let's analyse the results")
st.markdown('Here we generate plots that help us to understand how does the model fits with the data:')

with st.echo():

    r_columns = ['normalize', 'bias', 'interaction', 'intercept', 'degree']

    for c in r_columns:

        #fig = px.box(results, x=c, y="score_max", color=c , notched=True, title=c+' vs. score_max')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results['score_max'], x=results[c], 
                #data=housedata_plot,
                width=0.5,
                #palette="colorblind")
                palette="bright")
        st.pyplot()


st.success(round(time.time()-start_0,3))
st.write(alles_good_papi())
st.balloons()