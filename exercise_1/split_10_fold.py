import os
import os.path as path
from os.path import dirname as up
from unipath import Path
import pandas as pd
import sys
import numpy as np
import sklearn as sk
from sklearn.model_selection import KFold
from sklearn.model_selection import GroupKFold


np.random.seed(9103)
# READ FROM DATA DIR
# two_up = up(up(__file__))
current = Path(os.getcwd())
two_up = current.ancestor(1)
#print(two_up)
#exit()
df = pd.read_csv('.\cardio_1.csv', sep = ';', encoding='utf-8', dtype="str")

# THIS FUCNTION NEEDS TO BE CORRECTED:

cv = KFold(n_splits=10, random_state=9103, shuffle=True)
#cv = GroupKFold(n_splits=10) # This is for not overlapping groups

fold = 1
for train_index, test_index in cv.split(df):
    #df.iloc[train_index,:].to_csv('.\folds\cardio_train_'+str(fold)+'.csv' ,   sep=';', encoding='utf-8', index=False)
    #df.iloc[test_index,:].to_csv('.\folds\cardio_test_'+str(fold)+'.csv' ,   sep=';', encoding='utf-8', index=False)
    df.iloc[train_index,:].to_csv(current+'\\folds\cardio_train_'+str(fold)+'.csv' ,   sep=';', encoding='utf-8', index=False)
    df.iloc[test_index,:].to_csv(current+'\\folds\cardio_test_'+str(fold)+'.csv' ,   sep=';', encoding='utf-8', index=False)
    fold = fold+1

sys.exit()

'''
msk = np.random.rand(len(df)) <= 0.95
# Split the data into train and test

train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_1.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_1.csv',   sep=';', encoding='utf-8', index=False)

msk = np.random.rand(len(df)) <= 0.85
train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_2.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_2.csv',   sep=';', encoding='utf-8', index=False)

msk = np.random.rand(len(df)) <= 0.75
train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_3.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_3.csv',   sep=';', encoding='utf-8', index=False)

msk = np.random.rand(len(df)) <= 0.65
train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_4.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_4.csv',   sep=';', encoding='utf-8', index=False)

msk = np.random.rand(len(df)) <= 0.55
train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_5.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_5.csv',   sep=';', encoding='utf-8', index=False)

msk = np.random.rand(len(df)) <= 0.45
train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_6.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_6.csv',   sep=';', encoding='utf-8', index=False)

msk = np.random.rand(len(df)) <= 0.35
train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_7.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_7.csv',   sep=';', encoding='utf-8', index=False)

msk = np.random.rand(len(df)) <= 0.25
train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_8.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_8.csv',   sep=';', encoding='utf-8', index=False)

msk = np.random.rand(len(df)) <= 0.15
train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_9.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_9.csv',   sep=';', encoding='utf-8', index=False)

msk = np.random.rand(len(df)) <= 0.05
train = df[msk]
test = df[~msk]
train.to_csv(current+'\\folds\cardio_train_10.csv' ,   sep=';', encoding='utf-8', index=False)
test.to_csv(current+'\\folds\cardio_test_10.csv',   sep=';', encoding='utf-8', index=False)

sys.exit()
'''