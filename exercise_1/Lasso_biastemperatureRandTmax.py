import streamlit as st
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import os.path as path
from os.path import dirname as up
from unipath import Path
import random
import seaborn as sns
import sklearn as sk
import plotly.express as px
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Lasso
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
#from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import confusion_matrix
#from sklearn.metrics import confusion_matrix
import time
from joblib import Parallel, delayed
import multiprocessing

start_0 = time.time()
num_cores = multiprocessing.cpu_count()

def alles_good_papi():
    return("Alles good papi")

#Function that saves datasets
def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())




### LASSO REGRESION

def create_Lasso(models = []):
    normalize = [False, True]
    #bias = [False, True]
    #interaction = [False, True]
    intercept = [True, False]
    #order = ['C','F']
    alpha = [0, 1e-15, 1e-10, 1e-5, 1, 5, 10, 16, 32]
    #alpha = [0.000000001, 0.0001, 0.01, 0.1, 0.3, 0.5, 0.8, 1.0]
    alpha = [0, 1e-5, 1, 5, 10, 32]
    #degree = [ 1, 2, 3 ]

    ## TO TEST FASTER:
    #n_neighbors = [ 2, 4, 6]
    #algorithm = ["ball_tree", "kd_tree"]
    #weight = ["uniform"]

    # CREATE MODELS
    m_id = 1
    for n in normalize:
        for inter in intercept:
            for a in alpha:
                models.append([m_id,n,inter,a])
                m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models_Lasso(i):
    path = up(__file__)
    file_name = path+'\\folds\\biastemperatureRandTmax_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = path+'\\folds\\biastemperatureRandTmax_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['Next_Tmax'], axis=1)
    y_train = df_train['Next_Tmax']
    X_test = df_test.drop(['Next_Tmax'], axis=1)
    y_test = df_test['Next_Tmax']

    results = pd.DataFrame()
    models = create_Lasso([])
    #st.write(models)
    for m in models:
        start = time.time()
        result_row = {}
        m_id,n,inter,a = m[:] 
        result_row["fold"] = i
        result_row["model"] = m_id
        result_row["normalize"] = n
        result_row["intercept"] = inter
        result_row["alpha"] = a
        lasso = Lasso(alpha=a, fit_intercept=inter, normalize=n)
        lasso.fit(X_train, y_train)
        result_row["score"] = round(lasso.score(X_test, y_test), 6)
        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results

def aggregate_results_Lasso(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['normalize'] = results.groupby('model')['normalize'].max()
    results_folds['intercept'] = results.groupby('model')['intercept'].max()
    results_folds['alpha'] = results.groupby('model')['alpha'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()
    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds



### HERE WE JUST SHOW THE IMPLEMENTATION

st.markdown("# Machine Learning 2020S")
st.markdown("# Exercise 1 - Regression - Group 32")
st.markdown("## Lasso Regression - biastemperatureRandTmax")
st.markdown('The data set looks like:')

with st.echo():
    # READ FROM DATA DIR
    two_up = up(up(__file__))
    df_p = pd.read_csv(two_up+'\data\\biastemperatureRandTmax_0_clean.csv', sep = ';', encoding='utf-8')
    diamonds_clean = df_p#.iloc[1:100,:]
    desc = diamonds_clean.describe()

#st.write(two_up)
st.write(diamonds_clean)
st.write(diamonds_clean.shape)
st.write(desc)
st.write(desc.shape)



st.markdown("## Preprocessing the dataset for the Regression model")
st.markdown('First selecting some of the columns')


with st.echo():
    features = [
  "station",
  "Present_Tmax",
  "Present_Tmin",
  "LDAPS_RHmin",
  "LDAPS_RHmax",
  "LDAPS_Tmax_lapse",
  "LDAPS_Tmin_lapse",
  "LDAPS_WS",
  "LDAPS_LH",
  "LDAPS_CC1",
  "LDAPS_CC2",
  "LDAPS_CC3",
  "LDAPS_CC4",
  "LDAPS_PPT1",
  "LDAPS_PPT2",
  "LDAPS_PPT3",
  "LDAPS_PPT4",
  "lat",
  "lon",
  "DEM",
  "Slope",
  "Solar radiation",
  "Next_Tmax"
]

df = diamonds_clean.loc[:,features]


st.markdown("## Split the dataset for 10-Folds Cross-Validation")
st.markdown('The resulting datasets are store in the directory /folds')

with st.echo():
    current = Path(os.getcwd())
    cv = KFold(n_splits=10, random_state=9103, shuffle=True)
    #cv = GroupKFold(n_splits=10) # This is for not overlapping groups
    fold = 1
    for train_index, test_index in cv.split(df):
        df.iloc[train_index,:].to_csv(current+'\\folds\\biastemperatureRandTmax_train_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        df.iloc[test_index,:].to_csv(current+'\\folds\\biastemperatureRandTmax_test_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        fold = fold+1

st.markdown('One dataset for training looks like:')
st.write(df.iloc[train_index,:],df.iloc[train_index,:].shape)
st.markdown('One dataset for testing looks like:')
st.write(df.iloc[test_index,:],df.iloc[test_index,:].shape)

st.markdown('## Fitting the models!')
st.markdown('Here we fit the models parallelizing the cross-validation')

with st.echo():

    results = pd.DataFrame()

    arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_Lasso)(i)  for i in range (1,11))
    for a in arr_results:
        results = results.append(a, ignore_index = True)

    #for i in range (1,11):
    #   results = results.append(folds_fit_models_Lasso(i), ignore_index = True)
    
    results = aggregate_results_Lasso(results)    

    # EXPORT THE FILE
    current = up(__file__)
    file_name = current + '\\target\\biastemperature\\biastemperatureRandTmax_results_Lasso_folds.csv' # file name of the output file
    write(results,file_name)

st.markdown('Here we can see the results of the computation of the models with different parameters')
st.write(results,results.shape)

st.markdown("## Let's analyse the results")
st.markdown('Here we generate plots that help us to understand how does the model fits with the data:')

with st.echo():

    r_columns = ['normalize', 'intercept', 'alpha']

    for c in r_columns:

        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results['score_mean'], x=results[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.5,
                #palette="colorblind")
                palette="bright")
        st.pyplot()


st.success(round(time.time()-start_0,3))
#st.write(alles_good_papi())
st.balloons()