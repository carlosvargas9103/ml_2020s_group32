import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import os.path as path
from os.path import dirname as up
import random
import seaborn as sns
import streamlit as st
#Libraries

#Load dataset and describe
def works():
    return("Alles gut my friends")

st.markdown('# Preprocessing for Linear Regression')
st.markdown("## Importing the datasets - Fishtoxicity")
st.markdown('The data set looks like:')

with st.echo():
    one_up = up(__file__)
    three_up = up(up(up(__file__)))
    filename = three_up + "\data\\fishtoxicity_0.csv"
    df = pd.read_csv(filename,
     sep=";",
     encoding="utf-8")
    desc = df.describe()

st.write(df.head(20))
st.markdown("Shape of the dataset: ")
st.write(df.shape)
st.markdown("Description of the dataset: ")
st.write(desc)
st.markdown("Shape of dataset description")
st.write(desc.shape)

st.markdown("## Correlation")
st.markdown("Having collinearity problem afects mostly the precision of coefficient estimates. What's more, causes coefficients to be more sensitive to changes in the model. However, multicollinearity does NOT reduce the fit of the model. In other words, it does NOT reduce the score of the regression. In our work, we are trying to find the models that predict our target better. Thus, we do not care about collinearity so much. Anyway, we can quickly analyze it.")

with st.echo():
    correlation = df.corr(method="pearson")

st.markdown("Show correlation between columns: ")
st.write(correlation)

with st.echo():
    # Generate a mask for the upper triangle
    mask = np.triu(np.ones_like(correlation, dtype=np.bool))

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(15, 9))

    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(correlation, mask=mask, cmap=cmap, vmax=1, center=0, square=True, linewidths=.5, cbar_kws={"shrink": .5})
    plt.savefig(fname="plots\\fishtoxicity\correlation_matrix.png")
    st.pyplot()

st.markdown("Todo ok con las correlation!! Nada raro. No modificamos nada.")

# #Generating missing values
# st.markdown("## Generating Missing ")
# st.markdown("The dataset doesn't have missing values, we generate some ones randomly")

# with st.echo():
#     np.random.seed(28938547)
#     #create a boolean Numpy array of the same size diamonds DataFrame
#     #around percentage_missing of the elments are True
#     percentage_missing = 0.05
#     nan_mat = np.random.random(df.shape) < percentage_missing
    
#     #Pandas’ function mask to each element in the dataframe. 
#     #The mask function will use the element in the dataframe if the condition is False and change it to NaN if it is True.
#     #pd.mask(cond, other=nan)
#     df_NAN = df.mask(nan_mat)
#     df_NAN["price"] = df["price"] #Avoids the labelled attribute to have missing

#     total_missing_values = df_NAN.isnull().sum()
#     percentage_missing_values = total_missing_values/df_NAN.shape[0]

# st.markdown(f"The dataset does not have missing values. However, to make it more realistic we generate around of {percentage_missing*100} % of missing values over the columns.")
# st.markdown("Missing per column: ")
# st.write(total_missing_values)
# st.markdown("Percentage of missing values: ")
# st.write(percentage_missing_values)
# st.write("Now the dataset contains missing values: ")
# st.write(df_NAN.head(50))

st.markdown("## Variables")
st.markdown("The variables are: ")
with st.echo():
    #Now, let's jump into variables
    features_numerical_input = ["CIC0","SM1_Dz(Z)","GATS1i","MLOGP"]
    features_categorical_input = ["NdsCH","NdssC"]
    features_numerical_output = ["LC50 [-LOG(mol/L)]"]
    attributes_types = df.dtypes
    st.write(attributes_types) 


st.markdown('## Numerical variables')
st.markdown("Let's plot the numerical variables and see it's individual distribution")
with st.echo():
    #And now, we should be able to plot the distribution
    for i in features_numerical_input:
        sns.distplot(df[i]).set_title(f"Distribution of attribute: {i}")
        st.pyplot()

st.markdown("## Data analysis in depth")

with st.echo():
    #From the data inside the limits, we must check for posible outliers which reduce performance of our models
    for i in features_numerical_input:
        sns.boxplot(df[i]).set_title(f"Boxplot of attribute: {i}")
        st.pyplot()

st.markdown("## Outlier analysis")
with st.echo():
    #For CIC0
    outsideCIC0 = df[df["CIC0"] > 5]
    st.write(outsideCIC0)
    st.write(outsideCIC0.shape)
st.markdown("We drop these values")
list_to_drop = outsideCIC0.index.tolist()
df = df.drop(labels=list_to_drop)

with st.echo():
    #For GATS1i
    outsideGATS1i = df[df["GATS1i"] > 2.7]
    st.write(outsideGATS1i)
    st.write(outsideGATS1i.shape)
st.markdown("We drop these values")
list_to_drop = outsideGATS1i.index.tolist()
df = df.drop(labels=list_to_drop)

with st.echo():
    #For MLOGP
    outsideMLOGP= df[df["MLOGP"] < -2] 
    st.write(outsideMLOGP)
    st.write(outsideMLOGP.shape)
st.markdown("We drop these values")
list_to_drop = outsideMLOGP.index.tolist()
df = df.drop(labels=list_to_drop)

st.markdown('## Distribution and boxplot of each preprocessed variable')
with st.echo():
    for i in features_numerical_input:
        sns.distplot(df[i]).set_title(f"Distribution of attribute: {i}")
        plt.savefig(fname= f"plots\\fishtoxicity\distplot_{i}.png")
        st.pyplot()


with st.echo():
    for i in features_numerical_input:
        sns.boxplot(df[i]).set_title(f"Boxplot of attribute: {i}")
        plt.savefig(fname=f"plots\\fishtoxicity\plot_box{i}.png")
        st.pyplot()


st.markdown("## Finally, we save the df as fishtoxicity_0_clean: ")
st.markdown("Shape of dataset ")
st.write(df.shape)

with st.echo():
    df.to_csv(three_up + "\data\\fishtoxicity_0_clean.csv", sep=';', encoding='utf-8', index=False)

st.balloons()
st.write(works())