import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import os.path as path
from os.path import dirname as up
import random
import seaborn as sns
import streamlit as st
#Libraries

#Load dataset and describe
def works():
    return("Alles gut amigos!!")


st.markdown("# Preprocessing for linear regression")
st.markdown("## Importing the datasets - House Sales in King County, USA")
st.markdown('The data set looks like:')

with st.echo():
    one_up = up(__file__)
    three_up = up(up(up(__file__)))
    filename = three_up + "\data\housedata_0.csv"
    df = pd.read_csv(filename,
     sep=";",
     encoding="utf-8")
    desc = df.describe()

st.write(df.head(20))
st.markdown("Shape of the dataset: ")
st.write(df.shape)
st.markdown("Description of the dataset: ")
st.write(desc)
st.markdown("Shape of dataset description")
st.write(desc.shape)
st.markdown("Missing values:")
st.write(df.isnull().sum())
st.markdown("No missing. We keep going.")


#Drop useless columns for regression

st.markdown("## Deleting useless columns")

with st.echo():
    df = df.drop(["id","date", "zipcode"], axis=1)
    #id --> nonsense
    #data --> not our focus of analysis
    #location ones --> what do we do with them? lat y lon las miramos en correlation (latitud parece importante. longitud no tanto). Zipcode (podríamos hacer una categórica)


st.markdown("Checking correlation")

with st.echo():
    correlation = df.corr(method="pearson")


st.markdown("Show correlation between columns: ")
st.write(correlation)

##Plot the pearson correlation matrix !!
with st.echo():
    # Generate a mask for the upper triangle
    mask = np.triu(np.ones_like(correlation, dtype=np.bool))

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(15, 9))

    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(correlation, mask=mask, cmap=cmap, vmax=1, center=0, square=True, linewidths=.5, cbar_kws={"shrink": .5})
    plt.savefig(fname="plots\house\correlation_matrix.png")
    st.pyplot()

st.markdown("Deletable: long and condition. Do an alternative dataset without them later.")
st.write(correlation[correlation["price"] < 0.04])

#Characterize dataset
st.markdown("## Characterizing the datasets")
st.markdown('First, it is important to know which type of variables we have:')

with st.echo():
    attributes_types = df.dtypes
    st.write(attributes_types)


features_numerical = ["price","bedrooms","bathrooms","sqft_living","sqft_lot","floors","sqft_above","sqft_basement","yr_built","sqft_living15","sqft_lot15"]
features_categorical = ["waterfront","view","condition","grade"]

st.write("Although all of them look numerical. There are some which are in fact categorical like waterfront, view, condition or grade.")

#Numerical variables
st.markdown("We start by plotting numerical variables: ")

#Distributions
with st.echo():
    for i in features_numerical:
        sns.distplot(df[i]).set_title(f"Distribution of attribute: {i}")
        plt.savefig(fname=f"plots\house\distribution_{i}.png")
        st.pyplot()

#Checking outliers
st.markdown("In general, all the attributes' distributions show extremly long tails. Normally this could be due to the presence of outliers. However, for the case of housing prices, it makes sense that the distributions are very right-skewed due to the presence of less common luxury houses.")
st.markdown("It will be really costly to check whether some of those extreme values are actually outliers. Let's just quickly show the boxplots of this variables and check the REALLY extreme values.")

with st.echo():
    for i in features_numerical:
        sns.boxplot(df[i]).set_title(f"Boxplot of attribute: {i}")
        plt.savefig(fname=f"plots\house\plot_box{i}.png")
        st.pyplot()


with st.echo():
    st.write("Checking the price makes sense: minimum value (the maximum we don't care as explained before). ")
    st.write(df[df["price"] == min(df["price"])])
    st.write("It was sold for 75000. Makes sense.")
    st.write("Outstanding outliers in bedrooms")
    st.write(df[df["bedrooms"] > 30])
    st.write("Is's a 1947 house with above 1500 sqmt and sold by $640000 with no-renovation. May have 33 bedrooms. We assume is not a mistaken instance. We do not remove the value")
    st.write("The rest of values seem to hold in the hypothesis of not-so-common luxury houses.")



st.markdown("For regressions, where distance between points plays an important role, normalization/scaling/standarsation may be also needed. However, we will do that when developing our models. In fact, normalization will be one of the parameters with which we will experiment over the models. ")


#Categorical attributes
#Faltaría el boxplot de las categóricas

#One-hot-encoding

st.markdown("## One Hot Encoding")
st.markdown("Regression can only be performed with numerical variables. Thus, we need some transformation of our categorical variables. We will use the one-hot-encoding approach: ")

with st.echo():
    st.write("Waterfront is already one-hot-encoded. Now we do it for the other categorical variables.")
    df_hot_encoding = pd.get_dummies(df[["condition","grade","view"]].astype(str),prefix=["condition","grade","view"])
    # use pd.concat to join the new columns with your original dataframe
    df = pd.concat([df,df_hot_encoding],axis=1)
    # now drop the original columbs (we don't need it anymore)
    df = df.drop(["view","condition","grade"],axis=1)
    st.write(df.head(10))
    st.write(df.shape)

st.markdown("Finally, we save the df as housedata_0_clean: ")

with st.echo():
    df.to_csv(three_up + "\data\housedata_0_clean.csv", sep=';', encoding='utf-8', index=False)

with st.echo():
    df = df.drop(["condition_1","condition_2","condition_3","condition_4","condition_5","long"], axis=1)
    df.to_csv(three_up + "\data\housedata_0_clean_without_non-correlated_columns.csv", sep=';', encoding='utf-8', index=False)


st.balloons()
st.write(works())