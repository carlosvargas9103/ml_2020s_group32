import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import os.path as path
from os.path import dirname as up
import random
import seaborn as sns
import streamlit as st
#Libraries

#Load dataset and describe
def works():
    return("Alles gut amigos!!")


st.markdown("# Preprocessing for linear regression")
st.markdown("## Importing the datasets - Diamonds")
st.markdown('The data set looks like:')

with st.echo():
    one_up = up(__file__)
    three_up = up(up(up(__file__)))
    filename = three_up + "\data\diamonds_0.csv"
    df = pd.read_csv(filename,
     sep=";",
     encoding="utf-8")
    desc = df.describe()

st.write(df.head(20))
st.markdown("Shape of the dataset: ")
st.write(df.shape)
st.markdown("Description of the dataset: ")
st.write(desc)
st.markdown("Shape of dataset description")
st.write(desc.shape)
st.markdown("Missing Values:")
st.write(df.isnull().sum())
st.write("No missing values in this dataset. We don't need to impute them.")




#Drop useless columns for regression

st.markdown("## Deleting useless columns")
st.markdown("All columns of this dataset are useful for regression. However, some of them are correlated, maybe we  have multicolinearity problems. Multicollinearity occurs when the model includes variables that are correlated nut just with the target variable, but also with one another. That means, some columns may be redundant.")
st.markdown("Having collinearity problem afects mostly the precision of coefficient estimates. What's more, causes coefficients to be more sensitive to changes in the model. However, multicollinearity does NOT reduce the fit of the model. In other words, it does NOT reduce the score of the regression. In our work, we are trying to find the models that predict our target better. Thus, we do not care about collinearity so much. Anyway, we can quickly analyze it.")

with st.echo():
    correlation = df.corr(method="pearson")

st.markdown("Show correlation between columns: ")
st.write(correlation)

with st.echo():
    # Generate a mask for the upper triangle
    mask = np.triu(np.ones_like(correlation, dtype=np.bool))

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(15, 9))

    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(correlation, mask=mask, cmap=cmap, vmax=1, center=0, square=True, linewidths=.5, cbar_kws={"shrink": .5})
    plt.savefig(fname="plots\diamonds\correlation_matrix.png")
    st.pyplot()


st.markdown("x, y and z which correspond to different measures of the diamonds (width, depth and length) are highly correlated with each other. In case we wanted to have confidence on our estimates about them, it would be better to create a new variable by combination of these 3 columns or delete some of them for the purpose of analysis.")
st.markdown("Logically, these 3 are also correlated with carat (weight of the diamond). Same as before applies.")
st.markdown("This is a quick visual way of analyzing collinearlity. However, to do it properly we should use more effective techniques. I.e: VIF (Variance Inflation Factor).")
st.markdown("It's also important to point out that correlation between price and depth is extremly close to 0. This means that it's likely that depth variable explains really few about the diamond price. To avoid having useless information that weakens the efficiency of our model, it may be interesting to remove this variable from the dataset. We will save two different datasets: one with this variable and the other without. This way, we can compare the results. ")

#Characterize dataset
st.markdown("## Characterizing the datasets")
st.markdown('First, it is important to know which type of variables we have:')

with st.echo():
    attributes_types = df.dtypes
    st.write(attributes_types)

features_numerical = ["carat","depth","table","x","y","z","price"]
features_categorical = ["cut","color","clarity"]

#Numerical variables
st.markdown("We start by plotting numerical variables: ")

#Distributions
with st.echo():
    for i in features_numerical:
        sns.distplot(df[i]).set_title(f"Distribution of attribute: {i}")
        st.pyplot()

#Checking outliers
st.markdown("In general, all the attributes' distributions show extremly long tails. What's more, most of the data is centered around a narrow range. This may be due to the presence of outliers. Let's check for them.")
st.markdown("For that purpose, let's see the attributes' boxplots: ")

with st.echo():
    for i in features_numerical:
        sns.boxplot(df[i]).set_title(f"Boxplot of attribute: {i}")
        st.pyplot()


#Deep analysis
st.markdown("Analyzing outstanding outliers: ")

st.markdown("Depth outliers: ")

with st.echo():
    st.write(df[df["depth"] < 45])

with st.echo():
    st.write(df[df["depth"] > 75])

st.markdown("Looking at the other values, there is nothing that will make us think  that there is some mistake in data colection or that the values don't make sense. In fact, in the other attributes, they also have lower (higher) values in comparison to the mean. Thus, we keep these outliers with us. We consider them relevant.")

st.markdown("Table outliers: ")

with st.echo():
    st.write(df[df["table"] < 45])

with st.echo():
    st.write(df[df["table"] > 85])

st.markdown("As well as before, we don't find evidence showing that this data may be mistaken.")

st.markdown("x outliers: ")

with st.echo():
    st.write(df[df["x"] < 2])
    st.write(df[df["x"] < 2].shape)

st.markdown("In this case, we can observet that there are 8 observations that don't have 0's for the attributes x, y and z whic correspond to lenght, depth and width in mm. It can be either some missing data or some error in the data. Most likely, the second option. Why? Because they have data in the attribute depth which depends in fact on the values of x, y and z. Since we have so many observations and this could lead to confusions, we will drop this observations from the dataset.")

with st.echo():
    list_to_drop = df[df["x"] < 2].index.tolist()
    df = df.drop(labels=list_to_drop)

st.markdown("Once these outliers have been removed, we want to make sure that there are not more 0 values for y or z: ")

with st.echo():
    st.write(df[df["y"] < 1])
    st.write(df[df["y"] < 1].shape)
    st.write(df[df["z"] < 1])
    st.write(df[df["z"] < 1].shape)

st.markdown("Attribute y doesn't have any other 0 observations. But attribute z does (12). As well as before, we drop these observations from the data: ")

with st.echo():
    list_to_drop = df[df["z"] < 1].index.tolist()
    df = df.drop(labels=list_to_drop)

st.markdown("Checking higher outliers for y and z too: ")

with st.echo():
    st.write(df[df["y"] > 20])
    st.write(df[df["y"] > 20].shape)
    st.write(df[df["z"] > 20])
    st.write(df[df["z"] > 20].shape)

st.markdown("There are two upper-outliers for attribute y. The second one that we see in this table is probably a typing error and should have value 3.18 instead of 31.8. All the other attributes and the target variable have more or less average values. Thus, we drop this observation. For the first one, it is more difficult to judge since also x and z are above the average. However, a width of 58 mm for a diamond seems to be unrealistic. Specially, if we compare with all the others observations that we have. What's more, a diamond of 58 mm width should have one of the higher prices and this one is high but not one of the top (same applies for the weight). Thus, we drop this observation too.")

with st.echo():
    list_to_drop = df[df["y"] > 20].index.tolist()
    df = df.drop(labels=list_to_drop)

st.markdown("The outlier of z looks clearly like a mistake. It was probably a typing error and should be 3.18 instead of 31.8. All the other attributes and the target variable have more or less average values. Thus, we drop this observation: ")

with st.echo():
    list_to_drop = df[df["z"] > 20].index.tolist()
    df = df.drop(labels=list_to_drop)


st.markdown("## Plotting after outliers have been removed")
#Plotting

st.markdown("Let's see again the attributes' boxplots and distributions once some (mistaken) outliers have been removed: ")

with st.echo():
    for i in features_numerical:
        sns.distplot(df[i]).set_title(f"Distribution of attribute: {i}")
        plt.savefig(fname= f"plots\diamonds\distplot_{i}.png")
        st.pyplot()


with st.echo():
    for i in features_numerical:
        sns.boxplot(df[i]).set_title(f"Boxplot of attribute: {i}")
        plt.savefig(fname=f"plots\diamonds\plot_box{i}.png")
        st.pyplot()


st.markdown("For regressions, where distance between points plays an important role, normalization/scaling/standarsation may be also needed. However, we will do that when developing our models. In fact, normalization will be one of the parameters with which we will experiment over the models. ")


#Categorical attributes

st.markdown("Time for categorical variables. Let's show attributes' boxplots related to diamonds price: ")

with st.echo():
    
    for i in features_categorical:

        bplot=sns.boxplot(y=df[i], x=df['price'], 
                        width=0.8,
                        palette="colorblind").set_title(f"Boxplots of attribute: {i}")

        plt.savefig(fname=f"plots\diamonds\plot_box_{i}.png")
        st.pyplot()


#One-hot-encoding

st.markdown("## One Hot Encoding")
st.markdown("Regression can only be performed with numerical variables. Thus, we need some transformation of our categorical variables. We will use the one-hot-encoding approach: ")

with st.echo():
    df_hot_encoding = pd.get_dummies(df[["cut","color","clarity"]],prefix=['cut',"color","clarity"])
    # use pd.concat to join the new columns with your original dataframe
    df = pd.concat([df,df_hot_encoding],axis=1)
    # now drop the original columbs (we don't need it anymore)
    df = df.drop(["color","cut","clarity"],axis=1)
    st.write(df.head(10))
    st.write(df.shape)

st.markdown("Finally, we save the df as diamonds_0_clean: ")

with st.echo():
    df.to_csv(three_up + "\data\diamonds_0_clean.csv", sep=';', encoding='utf-8', index=False)


st.markdown("Save the dataset also without the column 'depth'(remeber it was not correlated with the price.")

with st.echo():
    df = df.drop("depth", axis=1)
    df.to_csv(three_up + "\data\diamonds_0_clean_without_non-correlated_columns.csv", sep=';', encoding='utf-8', index=False)

st.balloons()
st.write(works())