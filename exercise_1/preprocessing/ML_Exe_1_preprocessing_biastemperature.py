import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import os.path as path
from os.path import dirname as up
import random
import seaborn as sns
import streamlit as st
from _datetime import datetime, date
#Libraries

#Load dataset and describe
def works():
    return("Alles gut my friends")

st.markdown('# Preprocessing for Linear Regression')
st.markdown("## Importing the datasets - BiasTemperature")
st.markdown('The data set looks like:')
st.markdown("Shape of the dataset: ")


with st.echo():
    one_up = up(__file__)
    three_up = up(up(up(__file__)))
    filename = three_up + "\data\\biastemperature_0.csv"
    df = pd.read_csv(filename,
     sep=";",
     encoding="utf-8")
    desc = df.describe()

st.write(df.head(20))
st.markdown("Shape of the dataset: ")
st.write(df.shape)
st.markdown("Description of the dataset: ")
st.write(desc)
st.markdown("Shape of dataset description")
st.write(desc.shape)


st.markdown("## Correlation")
st.markdown("Having collinearity problem afects mostly the precision of coefficient estimates. What's more, causes coefficients to be more sensitive to changes in the model. However, multicollinearity does NOT reduce the fit of the model. In other words, it does NOT reduce the score of the regression. In our work, we are trying to find the models that predict our target better. Thus, we do not care about collinearity so much. Anyway, we can quickly analyze it.")

with st.echo():
    correlation = df.corr(method="pearson")

st.markdown("Show correlation between columns: ")
st.write(correlation)

with st.echo():
    # Generate a mask for the upper triangle
    mask = np.triu(np.ones_like(correlation, dtype=np.bool))

    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(15, 9))

    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)

    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(correlation, mask=mask, cmap=cmap, vmax=1, center=0, square=True, linewidths=.5, cbar_kws={"shrink": .5})
    plt.savefig(fname="plots\\biastemperature\correlation_matrix.png")
    st.pyplot()

st.markdown("We create alternative datasets based on correlation and target values below.")
st.markdown("For next_t_min, really low correlation for: ")
st.write(correlation[(correlation["Next_Tmin"] < 0.03) & (correlation["Next_Tmin"] > (-0.03))])
st.markdown("For next_t_max, really low correlation for: ")
st.write(correlation[(correlation["Next_Tmax"] < 0.03)  & (correlation["Next_Tmax"] > (-0.03))])


st.markdown("## Missing values: ")
st.markdown("The dataset already have missing values.")
with st.echo():
    #Then, what about missing values
    df_NAN = df.isna().sum()
    df_NAN_percent = 100 * df.isna().sum() / len(df)
    
st.markdown("Total missing values: ")
st.write(df_NAN)
st.markdown("Percentage of missing values: ")
st.write(df_NAN_percent)

  
st.markdown("## Variables")
st.markdown("The variables are: ")
with st.echo():
    #Now, let's jump into numerical variables
    features_numerical_input = ["station","Present_Tmax","Present_Tmin","LDAPS_RHmax","LDAPS_RHmin","LDAPS_Tmax_lapse","LDAPS_Tmin_lapse",
    "LDAPS_WS","LDAPS_LH","LDAPS_CC1","LDAPS_CC2","LDAPS_CC3","LDAPS_CC4","DEM","Slope","Solar radiation"]
    features_numerical2_input = ["LDAPS_PPT1","LDAPS_PPT2","LDAPS_PPT3","LDAPS_PPT4"]
    features_numerical_output = ["Next_Tmax", "Next_Tmin"]
    features_interval = ["Date"] 
    features = ["station","Present_Tmax","Present_Tmin","LDAPS_RHmax","LDAPS_RHmin","LDAPS_Tmax_lapse","LDAPS_Tmin_lapse",
    "LDAPS_WS","LDAPS_LH","LDAPS_CC1","LDAPS_CC2","LDAPS_CC3","LDAPS_CC4","DEM","Slope","Solar radiation","LDAPS_PPT1","LDAPS_PPT2","LDAPS_PPT3","LDAPS_PPT4", "lat", "lon","Next_Tmin", "Next_Tmax"]
    attributes_types = df.dtypes
    st.write(attributes_types)


st.markdown("## Check that all the values are within the specified ranges")

st.markdown("## Let's check the Date - Interval variables")
st.markdown("First, as we saw before, we have to labels without data, therefore we cannot assume if these were taken in the right dates, so we delete them")
with st.echo():
    #And now, we pass to interval variables
     st.write(df[df["Date"].isnull()])
     list_to_drop = df[df["Date"].isnull()].index.tolist()
     df = df.drop(labels = list_to_drop)
    
st.markdown("Now, for the rest of dates we also delete those outside the range we are studying.")
with st.echo():
    #The date limits are
    # From "30/06/2013"
    # To "30/08/2017"
    
    for f in features_interval:
        try:
            valid_date = df[f].apply(lambda x: datetime.strptime(x, '%Y/%m/%d'))
            if not (date(2013, 6, 30) <= valid_date <= date(2017, 8, 30)):
                list_to_drop = df[df["Date"]].index.tolist()
                st.write(list_to_drop)
                df = df.drop(labels = list_to_drop)
        except ValueError:
            st.write(print('Invalid date!'))

st.markdown("## Droping useless columns for regression")

with st.echo():
    st.write(" Date is not meaningful for this analysis.")
    df = df.drop(["Date"], axis=1)

st.markdown('## Numerical variables')       
st.markdown("We have to round the values of the columns to one decimal")
with st.echo():
    df_round = pd.DataFrame()
    for f in features_numerical_input:
        df_round[f] = round(df[f],1)

st.markdown("Checking that values are within the limits.")
with st.echo():
    for i in features_numerical_input:
        st.write(f"Min of {i}",min(df_round[i]), f"max of {i}", max(df_round[i]))
       
st.markdown("Let's plot the numerical variables and see it's individual distribution")
with st.echo():
    #And now, we should be able to plot the distribution
    for i in features_numerical_input:
        sns.distplot(df[i]).set_title(f"Distribution of attribute: {i}")
        st.pyplot()

st.markdown("## Data analysis in depth")

with st.echo():
    #From the data inside the limits, we must check for posible outliers which reduce performance of our models
    for i in features_numerical_input:
        sns.boxplot(df[i]).set_title(f"Boxplot of attribute: {i}")
        st.pyplot()


with st.echo():
    #For Slope
    outsideSlope = df[df["Slope"] > 3]
    st.write(outsideSlope)
    st.write(outsideSlope.shape)
st.markdown("Although the values bigger than 3 of slope are outside the boxplot. We have numerous samples that could be important to our model, so we keep them ")


with st.echo():
    #For LDAPS_WS
    outsideWS = df[df["LDAPS_WS"] > 18]
    st.write(outsideWS)
    st.write(outsideWS.shape)
st.markdown("Checking the 8 samples, most of them are taken from station number 20, and those have exact values in Elevation (DEM), that means redundancy so we drop those taken from station 20. The other two are rich in information so we keep them.")
list_to_drop = (outsideWS).index.tolist()
df = df.drop(labels=list_to_drop)

with st.echo():
    #For LDAPS_LH
    outsideLH = df[df["LDAPS_LH"] > 180]
    st.write(outsideLH)
    st.write(outsideLH.shape)
st.markdown("These 7 samples are rich in information and diversity, the only thing in commmon is higher values in temperature, but they are individually important, so we keep them.")


with st.echo():
    #For DEM
    outsideDEM = df[df["DEM"] > 175]
    st.write(outsideDEM)
    st.write(outsideDEM.shape)
st.markdown("These data, are picked up from high altitude points, in consequence, they are same samples related to Slope bigger than 3, we keep them")
   
st.markdown("## And now we save the new boxplots and distributions")
st.markdown('Distribution and boxplot of each preprocessed variable')
with st.echo():
    for i in features:
        sns.distplot(df[i]).set_title(f"Distribution of attribute: {i}")
        plt.savefig(fname= f"plots\\biastemperature\distplot_{i}.png")
        st.pyplot()


with st.echo():
    for i in features:
        sns.boxplot(df[i]).set_title(f"Boxplot of attribute: {i}")
        plt.savefig(fname=f"plots\\biastemperature\plot_box{i}.png")
        st.pyplot()

st.markdown("Shape of dataset ")
st.write(df.shape)

st.markdown("## Before finally exporting, we are creating different datasets variating the missing values and correlation")

st.markdown("Changing missing with label mean")
with st.echo():
    mean = pd.DataFrame()
    mean = df
    for f in features:
        df_mean = mean[f].mean()
        mean[f].fillna(df_mean, inplace = True)

st.markdown("Changing missing with random value")
with st.echo():
    rand = pd.DataFrame()
    rand = df
    for f in features:
        df_random = random.uniform(min(rand[f]),max(rand[f]))
        rand[f].fillna(df_random, inplace = True)

st.markdown("Deleting missing values")
with st.echo():
       df = df.dropna()

st.markdown('As we have two output variables, we divide the dataset into both Next_Tmax and Next_Tmin for computing easier regression')
st.markdown('Mean')
with st.echo():
    #Tmin
    mean_nextTmin = mean.drop("Next_Tmax", axis=1) #drop the other target
    mean_nextTmin.to_csv(three_up + "\data\\biastemperatureMeanTmin_0_clean.csv", sep=';', encoding='utf-8', index=False)
    #Tmin without non-correlated-to-the-target columns
    mean_nextTmin = mean_nextTmin.drop(["LDAPS_CC1","LDAPS_PPT1","LDAPS_PPT2"], axis=1) #drop non-correlated-to-the-target columns
    mean_nextTmin.to_csv(three_up + "\data\\biastemperatureMeanTmin_nocorr_0_clean.csv", sep=';', encoding='utf-8', index=False)
with st.echo():
    #Tmax
    mean_nextTmax = mean.drop("Next_Tmin", axis=1) #drop the other target
    mean_nextTmax.to_csv(three_up + "\data\\biastemperatureMeanTmax_0_clean.csv", sep=';', encoding='utf-8', index=False)
    #Tmin without non-correlated-to-the-target columns
    mean_nextTmax = mean_nextTmax.drop(["lon","Solar radiation"], axis=1) #drop non-correlated-to-the-target columns
    mean_nextTmax.to_csv(three_up + "\data\\biastemperatureMeanTmax_nocorr_0_clean.csv", sep=';', encoding='utf-8', index=False)

st.markdown('Random')
with st.echo():
    #Tmin
    rand_nextTmin = rand.drop("Next_Tmax", axis=1) #drop the other target
    rand_nextTmin.to_csv(three_up + "\data\\biastemperatureRandTmin_0_clean.csv", sep=';', encoding='utf-8', index=False)
    #Tmin without non-correlated-to-the-target columns
    rand_nextTmin = rand_nextTmin.drop(["LDAPS_CC1","LDAPS_PPT1","LDAPS_PPT2"], axis=1) #drop non-correlated-to-the-target columns
    rand_nextTmin.to_csv(three_up + "\data\\biastemperatureRandTmin_nocorr_0_clean.csv", sep=';', encoding='utf-8', index=False)
with st.echo():
    #Tmax
    rand_nextTmax = rand.drop("Next_Tmin", axis=1) #drop the other target
    rand_nextTmax.to_csv(three_up + "\data\\biastemperatureRandTmax_0_clean.csv", sep=';', encoding='utf-8', index=False)
    #Tmin without non-correlated-to-the-target columns
    rand_nextTmax = rand_nextTmax.drop(["lon","Solar radiation"], axis=1) #drop non-correlated-to-the-target columns
    rand_nextTmax.to_csv(three_up + "\data\\biastemperatureRandTmax_nocorr_0_clean.csv", sep=';', encoding='utf-8', index=False)

st.markdown('No Missing')
with st.echo():
    #Tmin
    df_nextTmin = df.drop("Next_Tmax", axis=1) #drop the other target
    df_nextTmin.to_csv(three_up + "\data\\biastemperatureNMTmin_0_clean.csv", sep=';', encoding='utf-8', index=False)
    #Tmin without non-correlated-to-the-target columns
    df_nextTmin = df_nextTmin.drop(["LDAPS_CC1","LDAPS_PPT1","LDAPS_PPT2"], axis=1) #drop non-correlated-to-the-target columns
    df_nextTmin.to_csv(three_up + "\data\\biastemperatureNMTmin_nocorr_0_clean.csv", sep=';', encoding='utf-8', index=False)
with st.echo():
    #Tmax
    df_nextTmax = df.drop("Next_Tmin", axis=1) #drop the other target
    df_nextTmax.to_csv(three_up + "\data\\biastemperatureNMTmax_0_clean.csv", sep=';', encoding='utf-8', index=False)
    #Tmin without non-correlated-to-the-target columns
    df_nextTmax = df_nextTmax.drop(["lon","Solar radiation"], axis=1) #drop non-correlated-to-the-target columns
    df_nextTmax.to_csv(three_up + "\data\\biastemperatureNMTmax_nocorr_0_clean.csv", sep=';', encoding='utf-8', index=False)


st.balloons()
st.write(works())