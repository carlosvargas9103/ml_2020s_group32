import streamlit as st
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import os.path as path
from os.path import dirname as up
from unipath import Path
import random
import seaborn as sns
import sklearn as sk
import plotly.express as px
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Lasso
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
#from sklearn.metrics import confusion_matrix
import time
from joblib import Parallel, delayed
import multiprocessing

start_0 = time.time()
num_cores = multiprocessing.cpu_count()
random_state = 9103
random.seed(9103)

def alles_good_papi():
    return("Alles good papi")

def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())





### RANDOM FOREST parameters

def create_RF(models = []):
    random_state = 9103
    #n_estimators = [1,2,3,5,10,32,100]
    n_estimators = [100]
    #max_depth = [6,18,32,None]
    max_depth = [2,5,6,None]
    min_samples_split = [1000]
    min_samples_leaf = [1]

    m_id = 1
    for n_e in n_estimators:
        for m_depth in max_depth:
            for s_s in min_samples_split:
                for s_l in min_samples_leaf:
                    models.append([m_id,n_e,m_depth,s_s,s_l])
                    m_id = m_id+1
    return models


### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models_RF(i):
    path = up(__file__)
    file_name = path+'\\folds\\biastemperatureMeanTmin_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = path+'\\folds\\biastemperatureMeanTmin_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['Next_Tmin'], axis=1)
    y_train = df_train['Next_Tmin']
    X_test = df_test.drop(['Next_Tmin'], axis=1)
    y_test = df_test['Next_Tmin']

    results = pd.DataFrame()
    models = create_RF([])
    #st.write(models)
    for m in models:
        start = time.time()
        result_row = {}
        m_id,n_e,m_depth,s_s,s_l = m[:] 
        result_row["fold"] = i
        result_row["model"] = m_id
        result_row["n_estimators"] = n_e
        result_row["max_depth"] = m_depth
        result_row["min_samples_split"] = s_s
        result_row["min_samples_leaf"] = s_l
        rfr = RandomForestRegressor(n_estimators=n_e, 
                                    max_depth=m_depth, 
                                    random_state=random_state, 
                                    criterion="mae", 
                                    min_samples_split=s_s, 
                                    min_samples_leaf=s_l)
        rfr.fit(X_train, y_train)
        result_row["score"] = round(rfr.score(X_test, y_test), 6)
        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results

def aggregate_results_RF(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['n_estimators'] = results.groupby('model')['n_estimators'].max()
    results_folds['max_depth'] = results.groupby('model')['max_depth'].max()
    results_folds['min_samples_split'] = results.groupby('model')['min_samples_split'].max()
    results_folds['min_samples_leaf'] = results.groupby('model')['min_samples_leaf'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()
    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds


### HERE WE JUST SHOW THE IMPLEMENTATION

st.markdown("# Machine Learning 2020S")
st.markdown("# Exercise 1 - Regression - Group 32")
st.markdown("## Random Forest Regression - biastemperatureMeanTmin")
st.markdown('The data set looks like:')

with st.echo():
    # READ FROM DATA DIR
    two_up = up(up(__file__))
    df_p = pd.read_csv(two_up+'\data\\biastemperatureMeanTmin_0_clean.csv', sep = ';', encoding='utf-8')
    diamonds_clean = df_p
    desc = diamonds_clean.describe()

#st.write(two_up)
st.write(diamonds_clean)
st.write(diamonds_clean.shape)
st.write(desc)
st.write(desc.shape)


st.markdown("Choose some feautres:")

with st.echo():
    features = [
  "station",
  "Present_Tmax",
  "Present_Tmin",
  "LDAPS_RHmin",
  "LDAPS_RHmax",
  "LDAPS_Tmax_lapse",
  "LDAPS_Tmin_lapse",
  "LDAPS_WS",
  "LDAPS_LH",
  "LDAPS_CC1",
  "LDAPS_CC2",
  "LDAPS_CC3",
  "LDAPS_CC4",
  "LDAPS_PPT1",
  "LDAPS_PPT2",
  "LDAPS_PPT3",
  "LDAPS_PPT4",
  "lat",
  "lon",
  "DEM",
  "Slope",
  "Solar radiation",
  "Next_Tmin"
]

df = diamonds_clean.loc[:,features]

st.markdown("## Split the dataset for 10-Folds Cross-Validation")
st.markdown('The resulting datasets are store in the directory /folds')

with st.echo():
    current = Path(os.getcwd())
    cv = KFold(n_splits=10, random_state=9103, shuffle=True)
    #cv = GroupKFold(n_splits=10) # This is for not overlapping groups
    fold = 1
    for train_index, test_index in cv.split(df):
        df.iloc[train_index,:].to_csv(current+'\\folds\\biastemperatureMeanTmin_clean_train_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        df.iloc[test_index,:].to_csv(current+'\\folds\\biastemperatureMeanTmin_clean_test_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        fold = fold+1

st.markdown('One dataset for training looks like:')
st.write(df.iloc[train_index,:],df.iloc[train_index,:].shape)
st.markdown('One dataset for testing looks like:')
st.write(df.iloc[test_index,:],df.iloc[test_index,:].shape)

st.markdown('## Fitting the models!')
st.markdown('Here we fit the models parallelizing the cross-validation')

with st.echo():

    results = pd.DataFrame()

    arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_RF)(i)  for i in range (1,11))
    for a in arr_results:
        results = results.append(a, ignore_index = True)

    #for i in range (1,11):
    #   results = results.append(folds_fit_models_RF(i), ignore_index = True)
    
    results = aggregate_results_RF(results)    

    # EXPORT THE FILE
    current = up(__file__)
    file_name = current + '\\target\\biastemperature\\biastemperatureMeanTmin_results_RF_folds.csv' # file name of the output file
    write(results,file_name)

st.markdown('Here we can see the results of the computation of the models with different parameters')
st.write(results,results.shape)

st.markdown("## Let's analyse the results")
st.markdown('Here we generate plots that help us to understand how does the model fits with the data:')

with st.echo():

    r_columns = ['n_estimators', 'max_depth', 'min_samples_split','min_samples_leaf']

    for c in r_columns:

        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results['score_mean'], x=results[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.5,
                #palette="colorblind")
                palette="bright")
        st.pyplot()


st.success(round(time.time()-start_0,3))
#st.write(alles_good_papi())
st.balloons()