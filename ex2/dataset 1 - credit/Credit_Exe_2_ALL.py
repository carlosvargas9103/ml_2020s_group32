#Libraries
import streamlit as st
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import os.path as path
from os.path import dirname as up
from unipath import Path
import random
import seaborn as sns
import sklearn as sk
import plotly.express as px
from sklearn import preprocessing
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Lasso
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import RidgeClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_classif
from sklearn.preprocessing import StandardScaler as st_scaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.svm import SVC


# Evaluation
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import fbeta_score
from sklearn.metrics import roc_auc_score
from sklearn.metrics import log_loss



#from sklearn.metrics import confusion_matrix
import time
from joblib import Parallel, delayed
import multiprocessing

#timing of the algorithms
start_0 = time.time()
num_cores = multiprocessing.cpu_count()
random_state = 9103
random.seed(9103)

#############
## HELPERS ##
#############

def df_reduce(df,percent):
    percent = float(percent)
    if percent == 1:
        return df
    ind = random.randint(0,len(df))
    index_r = random.sample(range(0,len(df)), int(len(df)*percent))
    df_r = df.iloc[index_r,:]
    return df_r

def scale_minMax(X):
    X_ = []
    scaler = MinMaxScaler()
    X_ = scaler.fit(X)
    X_ = scaler.transform(X)        
    return X_

###################
### CLASSIFIERS ###
###################

### RANDOM FOREST CLASSIFIER ###

def create_RF(models = []):
    f = 3
    fibo = [1, 2, 3, 5, 8, 13, 21, 23, 34, 55, 89, 144, 233, 377, 610, 987] # 15 positions
    random_state = 9103
    n_estimators = [1, 2, 3, 5, 8, 13, 21 ]#, 34, 55, 89, 144, 233]
    n_estimators = fibo[f+f:f+f+f+f]
    min_samples_leaf = fibo[:f+f] #[fibo[0]] #fibo[:f*2]
    #after trial and error
    k_features = [3,8,12,15]

    scale = [False,True]

    m_id = 1
    for n_e in n_estimators:
        #for m_depth in max_depth:
            #for s_s in min_samples_split:
                for s_l in min_samples_leaf:
                    for k in k_features:
                        for s in scale:
                            models.append([m_id,n_e,s_l,k,s])
                            m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET

def folds_fit_models_RF(i):
    #path = up(__file__)
    file_name = 'data\\folds\credit_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = 'data\\folds\credit_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['class'], axis=1)
    y_train = df_train['class']
    X_test = df_test.drop(['class'], axis=1)
    y_test = df_test['class']
    columns = X_train.columns

    results = pd.DataFrame()
    models = create_RF([])
    #st.write(models)
    #k=9
    for m in models:
        start = time.time()
        result_row = {}
        #m_id,n_e,m_depth,s_s,s_l,k = m[:]
        m_id,n_e,s_l,k,s = m[:]
        #st.write(m[:])
        result_row["fold"] = i
        result_row["model"] = str(m_id) + '_RFC'
        result_row["n_estimators"] = n_e
        #result_row["max_depth"] = m_depth
        #result_row["min_samples_split"] = s_s
        result_row["min_samples_leaf"] = s_l
        result_row["k_features"] = k
        result_row["scale"] = s

        #X_test = SelectKBest(chi2, k=k).fit_transform(X_test, y_test)
        selector = SelectKBest(f_classif, k=k)

        if s:
            X_train_s = scale_minMax(X_train)
            X_test_s = scale_minMax(X_test)

            X_train_r = selector.fit_transform(X_train_s, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test_s[:,k_features]

            #st.write(X_train_s,X_train_s.shape)
            #st.write(X_test,X_test.shape)
            #break
        else:
            X_train_r = selector.fit_transform(X_train, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test.iloc[:,k_features]
            #st.write(X_train,X_train.shape)
            #st.write(X_test,X_test.shape)
            #break
        
        rfr = RandomForestClassifier(n_estimators=n_e, 
                                    #max_depth=m_depth, 
                                    random_state=random_state, 
                                    criterion="gini", 
                                    #min_samples_split=s_s, 
                                    min_samples_leaf=s_l)
        rfr.fit(X_train_r, y_train)

        y_pred = rfr.predict(X_test_r)
        y_true = y_test

        result_row["score"] = round(rfr.score(X_test_r, y_test), 6)
        result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)
        result_row["fbeta_score"] = round(fbeta_score(y_true,y_pred, beta=5), 6)

        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results

## AGGREGATE THE DATA INTO A TABLE

def aggregate_results_RF(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['scale'] = results.groupby('model')['scale'].max()
    results_folds['n_estimators'] = results.groupby('model')['n_estimators'].max()
    #results_folds['max_depth'] = results.groupby('model')['max_depth'].max()
    #results_folds['min_samples_split'] = results.groupby('model')['min_samples_split'].max()
    results_folds['min_samples_leaf'] = results.groupby('model')['min_samples_leaf'].max()
    results_folds["k_features"] = results.groupby('model')['k_features'].max()
    
    results_folds['fbeta_score_mean'] = results.groupby('model')['fbeta_score'].mean()
    results_folds['fbeta_score_max'] = results.groupby('model')['fbeta_score'].max()
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()

    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds

def plot_RF(results_RF):
    r_columns = ['scale','n_estimators','min_samples_leaf','k_features']
    for c in r_columns:
        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_RF['score_mean'], x=results_RF[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )
        st.pyplot()
    return results_RF

### SUPPORT VECTOR MACHINE ###
## https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html

def create_SVM(models = []):
    f = 3
    fibo = [1, 2, 3, 5, 8, 13, 21, 23, 34, 55, 89, 144, 233, 377, 610, 987] # 15 positions

    #Specifies the kernel type to be used in the algorithm. 
    #kernel = ['linear','poly','rbf','sigmoid']
    #kernel = ['linear','poly','rbf']
    kernel = ['rbf']
   
    
    #Regularization parameter --> The strength of the regularization is inversely proportional to C. Must be strictly positive.
    C = fibo[:f+1]
    
    #Degree of the polynomial kernel function (‘poly’). Ignored by all other kernels.
    degree = fibo[:f+1] #fibo[f+f:f+f+f+f]
    
    #gamma = ['scale','auto']
    #coef0 = [0,0.003,0.03,0.3,0.6,1,3,9]
    #data scaled or not scaled
    scale = [False,True]
    k_features = [5,10,15,16] 
    
   

    m_id = 1
    for ker in kernel:
        for c in C:
            for d in degree:
                #for g in gamma:
                    #for c0 in coef0:
                        for s in scale:
                            for k_f in k_features:
                                #models.append([m_id,ker,c,d,g,c0,s,k_f])
                                models.append([m_id,ker,c,d,s,k_f])
                                m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET

def folds_fit_models_SVM(i):
    #path = up(__file__)
    file_name = 'data\\folds\credit_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = 'data\\folds\credit_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['class'], axis=1)
    y_train = df_train['class']
    X_test = df_test.drop(['class'], axis=1)
    y_test = df_test['class']
    columns = X_train.columns

    results = pd.DataFrame()
    models = create_SVM([])
    #st.write(models)
    #k=9

    for m in models:
        start = time.time()
        result_row = {}
        #m_id,ker,c,d,g,c0,s,k_f = m[:]
        m_id,ker,c,d,s,k_f = m[:]

        if ker != 'poly':
            d=0

        #st.write(m[:])
        result_row["fold"] = i
        result_row["model"] = str(m_id) + '_SVM'
        result_row["kernel"] = ker
        result_row["C"] = c
        result_row["degree"] = d
        #result_row["gamma"] = g
        #result_row["coef0"] = c0
        result_row["scale"] = s
        result_row["k_features"] = k_f

        #X_test = SelectKBest(chi2, k=k).fit_transform(X_test, y_test)
        selector = SelectKBest(f_classif, k=k_f)

        if s:
            X_train_s = scale_minMax(X_train)
            X_test_s = scale_minMax(X_test)

            X_train_r = selector.fit_transform(X_train_s, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test_s[:,k_features]

            #st.write(X_train_s,X_train_s.shape)
            #st.write(X_test,X_test.shape)
            #break
        else:
            X_train_r = selector.fit_transform(X_train, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test.iloc[:,k_features]
            #st.write(X_train,X_train.shape)
            #st.write(X_test,X_test.shape)
            #break

        if ker != 'poly':
            #d=0
            svm = SVC(
                C=c, 
                kernel=ker, 
                #degree=d, 
                #gamma=g, 
                #coef0=c0,
                random_state=random_state,
                probability = True
                )
        else:
            svm = SVC(
                C=c, 
                kernel=ker, 
                degree=d, 
                #gamma=g, 
                #coef0=c0,
                random_state=random_state,
                probability = True
                )

        svm.fit(X_train_r, y_train)

        y_pred = svm.predict(X_test_r)
        y_true = y_test
        y_scores = svm.predict_proba(X_test_r)

        result_row["score"] = round(svm.score(X_test_r, y_test), 6)
        result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)
        #roc_auc_scores: Compute Area Under the Receiver Operating Characteristic Curve (ROC AUC) from prediction scores.
        result_row["roc_auc_score"] = round(roc_auc_score(y_true, y_scores[:,1]),6)
        #adding log-loss
        result_row["log_loss"] = round(log_loss(y_true, y_scores), 6)
        result_row["fbeta_score"] = round(fbeta_score(y_true,y_pred, beta=5), 6)
        
        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results

## AGGREGATE THE DATA INTO A TABLE

def aggregate_results_SVM(results):
    results_folds = pd.DataFrame()

    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['scale'] = results.groupby('model')['scale'].max()
    results_folds['kernel'] = results.groupby('model')['kernel'].max()
    results_folds['C'] = results.groupby('model')['C'].max()
    results_folds['degree'] = results.groupby('model')['degree'].max()
    #results_folds['gamma'] = results.groupby('model')['gamma'].max()
    #results_folds["coef0"] = results.groupby('model')['coef0'].max()
    results_folds["k_features"] = results.groupby('model')['k_features'].max()

    ## Evaluation Metrics
    results_folds['fbeta_score_mean'] = results.groupby('model')['fbeta_score'].mean()
    results_folds['fbeta_score_max'] = results.groupby('model')['fbeta_score'].max()
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()
    #adding roc_auc_scores
    results_folds["roc_auc_score_mean"] = results.groupby('model')["roc_auc_score"].mean()
    results_folds["roc_auc_score:max"] = results.groupby('model')["roc_auc_score"].max()
    #adding log loss
    results_folds['log_loss_mean'] = results.groupby('model')['log_loss'].mean()
    results_folds['log_loss_max'] = results.groupby('model')['log_loss'].max()

    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds


def plot_SVM(results_SVM):
    r_columns = ['scale','kernel','C','degree','gamma','coef0','k_features']
    r_columns = ['scale','kernel','C','degree','k_features']
    for c in r_columns:
        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_SVM['score_mean'], x=results_SVM[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )
        st.pyplot()
    return results_SVM

### RIDGE CLASSIFIER ####
### https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.RidgeClassifier.html

def create_Ridge(models = []):
    normalize = [False, True]
    #bias = [False, True]
    #interaction = [False, True]
    intercept = [True, False]
    #order = ['C','F']
    alpha = [0, 1e-15, 1e-10, 1e-5, 1, 5, 10, 15]
    #alpha = [0.000000001, 0.0001, 0.01, 0.1, 0.3, 0.5, 0.8, 1.0]
    #degree = [ 1, 2, 3 ]
    #scale = [False,True]
    ## TO TEST FASTER:
    #n_neighbors = [ 2, 4, 6]
    #algorithm = ["ball_tree", "kd_tree"]
    #weight = ["uniform"]

    # CREATE MODELS
    m_id = 1
    for n in normalize:
        for inter in intercept:
            for a in alpha:
                models.append([m_id,n,inter,a])
                m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models_Ridge(i):
    file_name = 'data\\folds\credit_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = 'data\\folds\credit_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['class'], axis=1)
    y_train = df_train['class']
    X_test = df_test.drop(['class'], axis=1)
    y_test = df_test['class']
    columns = X_train.columns

    results = pd.DataFrame()
    models = create_Ridge([])


    for m in models:

        start = time.time()
        result_row = {}
        m_id,n,inter,a = m[:] 


        result_row["fold"] = i
        result_row["model"] = str(m_id) + '_Ridge'
        result_row["normalize"] = n
        result_row["intercept"] = inter
        result_row["alpha"] = a

        ridge = RidgeClassifier(alpha=a, fit_intercept=inter, normalize=n)
        ridge.fit(X_train, y_train)

        y_pred = ridge.predict(X_test)
        y_true = y_test

        result_row["score"] = round(ridge.score(X_test, y_test), 6)
        result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)
        result_row["fbeta_score"] = round(fbeta_score(y_true,y_pred, beta=5), 6)


        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results


def aggregate_results_Ridge(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['normalize'] = results.groupby('model')['normalize'].max()
    results_folds['intercept'] = results.groupby('model')['intercept'].max()
    results_folds['alpha'] = results.groupby('model')['alpha'].max()

    ## Evaluation Metrics
    results_folds['fbeta_score_mean'] = results.groupby('model')['fbeta_score'].mean()
    results_folds['fbeta_score_max'] = results.groupby('model')['fbeta_score'].max()
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()


    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)

    return results_folds


def plot_Ridge(results_ridge):
    r_columns = ['normalize','alpha','intercept']
    for c in r_columns:
        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_ridge['score_mean'], x=results_ridge[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )
        st.pyplot()
    return results_ridge

####################################################
### HERE WE JUST SHOW THE IMPLEMENTATION VERBOSE ###
####################################################

st.markdown("# Machine Learning 2020S")
st.markdown("# Exercise 2 - Classification - Group 32")
#Load dataset and describe
st.markdown("## Importing the datasets - Credit")
st.markdown('The data set looks like:')


with st.echo():

    filename = "data/credit_0.csv"
    df = pd.read_csv(filename,
     sep=";",
     encoding="utf-8")
    desc = df.describe()

st.write(df.head(20))
st.markdown("Shape of the dataset: ")
st.write(df.shape)
st.markdown("Description of the dataset: ")
st.write(desc)
st.markdown("Shape of dataset description")
st.write(desc.shape)


st.markdown("## Correlation")
with st.echo():
    correlation = df.corr(method="pearson")

st.markdown("Show correlation between columns: ")
st.write(correlation)

with st.echo():
    # Generate a mask for the upper triangle
    mask = np.triu(np.ones_like(correlation, dtype=np.bool))
    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(15, 9))
    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    # Draw the heatmap with the mask and correct aspect ratio
    sns.heatmap(correlation, mask=mask, cmap=cmap, vmax=1, center=0, square=True, linewidths=.5, cbar_kws={"shrink": .5})
    st.pyplot()
st.markdown("We dont percieve critical correlation problems within labels, therefore we maintain all attributes.")

st.markdown("## Missing values: ")
st.markdown("Do we have?")
with st.echo():
    #Then, what about missing values
    df_NAN = df.isna().sum()
    df_NAN_percent = 100 * df.isna().sum() / len(df)
    
st.markdown("NO, Total missing values: ")
st.write(df_NAN.sum())


st.markdown("## The variables are")
with st.echo():
    features_categorical= ["checking_status","credit_history","purpose","savings_status","employment","personal_status","other_parties",
 "property_magnitude","other_payment_plans","housing","job","own_telephone","foreign_worker"]
    features_numerical = ["duration","credit_amount", "installment_commitment", "residence_since","age","existing_credits","num_dependents"]
    target = ["class"]

st.markdown('## Deal with numerical')
# with st.echo():
#     for i in features_numerical:
#         sns.distplot(df[i]).set_title(f"Distribution of attribute: {i}")
#         st.pyplot()
# with st.echo():
#     for i in features_numerical:
#         sns.boxplot(df[i]).set_title(f"Boxplot of attribute: {i}")
#         st.pyplot()

st.markdown("Depth outliers: ")

with st.echo():
    st.write(df[df["duration"] > 65])
    # We find that this person is classified as bad at credit risks, reviewing his information we see he has 24 years old, only owns a car and created his account at the age of 18, is a good sample for the study so we keep it
    st.write(df[df["credit_amount"] > 17000])
    # This one is weird, because it's classified as bad at credit risks, although having the biggest amount so far, nevertheless we keep it.
    st.write(df[df["age"] > 70])
    # Nothing to declare
    st.write(df[df["existing_credits"] > 3.5])
    #These ones are risk clients overall, note that all foreigners

st.markdown('## What types of data we have?')
st.markdown('Only object and numerical data')
st.write(df.dtypes)

st.markdown('## Deal with categorical')
st.markdown('First, we have to do label enconding to our target "class" ')
with st.echo():
    df_LE = preprocessing.LabelEncoder()
    df[['class']] = df_LE.fit_transform(df[['class']])
    st.write(df.head(5))
    st.write(df.shape)

# with st.echo():
#     for i in features_categorical:
#         bplot=sns.boxplot(y=df[i], x=df['class'], 
#                         width=0.8,
#                         palette="colorblind").set_title(f"Boxplots of attribute: {i}")
#         st.pyplot()
st.markdown(' And label encoding to those categorical variables possible ')
with st.echo():
    df[['own_telephone']] = df_LE.fit_transform(df[['own_telephone']])
    df[['foreign_worker']] = df_LE.fit_transform(df[['foreign_worker']])
    st.write(df.head(5))
    st.write(df.shape)

st.markdown("For the rest of categorical data: We create correlation matrix for each value and assign distances depending on their results, for that reason we can build some premises to work with that will be founded on something")
with st.echo():
    output = df[['class']].copy()
    corr = pd.DataFrame(output)
    for f in features_categorical: 
        corr_hot_encoding = pd.get_dummies(df[[f]])
        corr = pd.concat([corr,corr_hot_encoding],axis=1)
        ix = corr.corr(method='pearson').sort_values('class', ascending=False).index
        corr_sorted = corr.loc[:, ix]
        # corr_cat = corr.corr(method="pearson")
        # st.write(corr_cat)
        corr = corr[['class']]

st.markdown("From the rest of categorical values, we have to options to classify, either find and replace or one-hot encoding.")
st.markdown("We have to based upon some premises in order to decide each independent values, using previous sorting correlation matrix.")
with st.echo():
    #Apply distances
    cleanup_nums = {"employment": {"'4<=X<7'": 5, "'>=7'": 4, "'1<=X<4'": 3, "unemployed": 2, "'<1'": 1},
                    "job": {"'unskilled resident'": 4, "skilled": 3, "'unemp/unskilled non res'": 2, "'high qualif/self emp/mgmt'": 1},
                      "housing": {"own": 3, "'for free'": 2, "rent":1},
                      "other_payment_plans": {"none": 3, "stores": 2, "bank":1},
                      "property_magnitude": {"'real estate'": 4, "'life insurance'": 3, "car": 2, "'no known property'":1},
                      "other_parties": {"guarantor": 3, "none": 2, "'co applicant'": 1},
                      "savings_status": {"'no known savings'": 5, "'>=1000'": 4, "'500<=X<1000'": 3, "'100<=X<500'": 2, "'<100'": 1},
                      "checking_status": {"'no checking'": 4, "'>=200'": 3, "'0<=X<200'": 2, "'<0'": 1},
                    "credit_history" : {"'all paid'": 5, "'no credits/all paid'": 4, "'existing paid'": 3, "'delayed previously'": 2, "'critical/other existing credit'": 1}
    }                    
    df.replace(cleanup_nums, inplace=True)
    st.write(df.head(10))
    st.write(df.shape)

st.markdown(' Then apply one-hot encoding to categorical variables with uncertainty of using distance correlation')
with st.echo():
    df_hot_encoding = pd.get_dummies(df[["purpose","personal_status"]],prefix=["purpose","personal_status"])
    # use pd.concat to join the new columns with your original dataframe
    df = pd.concat([df,df_hot_encoding],axis=1)
    # now drop the original columbs (we don't need it anymore)
    df = df.drop(["purpose","personal_status"],axis=1)
    st.write(df.head(10))
    st.write(df.shape)

#end of Preprocessing

st.markdown("## Characterizing of the datasets after preprocessing")
st.markdown('Here we generate some plots to understand better the data and the distribution of their features:')
# with st.echo():
#     for i in df.columns:
#         ax = sns.countplot(x=df[i], hue="class", data=df)
#         st.pyplot(clear_figure=True)

#Apply Cross-Validation
st.markdown("## Split the dataset for 10-Folds Cross-Validation")
st.markdown('The resulting datasets are store in the directory /folds')

with st.echo():
    cv = KFold(n_splits=10, random_state=9103, shuffle=True)
    #cv = GroupKFold(n_splits=10) # This is for not overlapping groups
    fold = 1
    for train_index, test_index in cv.split(df):
        df.iloc[train_index,:].to_csv('data\\folds\credit_train_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        df.iloc[test_index,:].to_csv('data\\folds\credit_test_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        fold = fold+1

st.markdown('One dataset for training looks like:')
st.write(df.iloc[train_index,:],df.iloc[train_index,:].shape)
st.markdown('One dataset for testing looks like:')
st.write(df.iloc[test_index,:],df.iloc[test_index,:].shape)

st.markdown('## Fitting the models!')
st.markdown('Here we fit the models parallelizing the cross-validation')

with st.echo():

    mit_allem = True
    classifiers_models = ['RF','Ridge','SVM']

    #PROGRESS BAR HERE!!!

    for c_m in classifiers_models:
        results = pd.DataFrame()
        results_c = pd.DataFrame()
        st.markdown("## "+c_m)
        if c_m == 'RF':
            if mit_allem:
                arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_RF)(i)  for i in range (1,11))
                for a in arr_results:
                    results = results.append(a, ignore_index = True)
            else:
                for i in range (1,11):
                    results = results.append(folds_fit_models_RF(i), ignore_index = True)
            
            st.markdown("## Results")
            st.markdown('Aggregated Table and Plots:')
            results_c = aggregate_results_RF(results)
            st.write(results_c,results_c.shape)
            plot_RF(results_c)

        elif c_m == 'SVM':
            if mit_allem:
                arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_SVM)(i)  for i in range (1,11))
                for a in arr_results:
                    results = results.append(a, ignore_index = True)
            else:
                for i in range (1,11):
                    results = results.append(folds_fit_models_SVM(i), ignore_index = True)
            
            st.markdown("## Results")
            st.markdown('Aggregated Table and Plots:')
            results_c = aggregate_results_SVM(results)
            st.write(results_c,results_c.shape)
            plot_SVM(results_c)
        
        elif c_m == 'Ridge':
            if mit_allem:
                arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_Ridge)(i)  for i in range (1,11))
                for a in arr_results:
                    results = results.append(a, ignore_index = True)
            else:
                for i in range (1,11):
                    results = results.append(folds_fit_models_Ridge(i), ignore_index = True)
            
            st.markdown("## Results")
            st.markdown('Aggregated Table and Plots:')
            results_c = aggregate_results_Ridge(results)
            st.write(results_c,results_c.shape)
            plot_Ridge(results_c)

        # EXPORT THE TABLE
        file_name = 'target\credit_results_'+c_m+'.csv' # name of the output file
        df.to_csv(file_name, sep=';', encoding='utf-8', index=False)


st.success(round(time.time()-start_0,3))



st.balloons()