import streamlit as st
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import os.path as path
from os.path import dirname as up
from unipath import Path
import random
import seaborn as sns
import sklearn as sk
import plotly.express as px
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.preprocessing import StandardScaler as st_scaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.svm import SVC

# Evaluation
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score


#from sklearn.metrics import confusion_matrix
import time
from joblib import Parallel, delayed
import multiprocessing
#from fancyimpute import KNN
from sklearn import preprocessing
#from sklearn.metrics import roc_auc_score
from sklearn.linear_model import RidgeClassifier
from sklearn.metrics import log_loss


start_0 = time.time()
num_cores = multiprocessing.cpu_count()
random_state = 9103
random.seed(9103)


def alles_good_papi():
    return("Alles good papi")

def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())

def generate_missing(dataset):
    percentage = 0.2 # percentage of values to replace in one column or randomly across all attributes
    feature = None # column name to replace values only for a specific column e.g.: 'LB' or None to replaces values across all feature attributes
    random_seed = 9103 # initialization parameter of the random number generator
    random_seed_iteration = 1
    file_name = path + '\data\cgt_missing_'+percentage+'.csv' # file name of the output file
    
    random.seed(random_seed)
    #dataset = read()
    columne_names = list(dataset.columns.values)
    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'Width',
                'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean', 'Median', 'Variance', 'Tendency']
    number_of_features = len(features)
    number_of_rows = len(dataset.index)
    num_rxf = number_of_rows * number_of_features

    st.write("Number of columns: %d" % len(columne_names))
    st.write("Number of (real) features: %d" % number_of_features)
    st.write("Number of rows: %d" % number_of_rows)
    st.write("Number of feature values: %d" % num_rxf)

    if feature is None:
        number_of_missing_values = int(num_rxf * percentage)
        st.write("Number of feature values to replace with NaN: %d" % number_of_missing_values)
        for x in range(number_of_missing_values): 
            random_seed = random_seed + random_seed_iteration
            random.seed(random_seed)
            random_column = random.choice(features) # only replace values with NaN for real features
            dataset.loc[dataset.sample(n=1, random_state=random_seed).index, random_column] = np.NaN
        write(dataset, file_name)
    else:
        dataset_percentage = dataset.sample(frac=percentage, random_state=random_seed)
        data_rest = dataset.drop(dataset_percentage.index)
        number_of_missing_values_for_column = len(dataset_percentage.index)
        st.write("Number of feature value for column '%s' to replace with NaN: %d" % (feature, number_of_missing_values_for_column))
        dataset_percentage[feature] = np.NaN
        result = pd.concat([data_rest, dataset_percentage])
        result.sort_index(inplace=True) # sort by index
        write(result, file_name)
        
    return(alles_good_papi())

#############
## HELPERS ##
#############

def df_reduce(df,percent):
    percent = float(percent)
    if percent == 1:
        return df
    ind = random.randint(0,len(df))
    index_r = random.sample(range(0,len(df)), int(len(df)*percent))
    df_r = df.iloc[index_r,:]
    return df_r

def scale_minMax(X):
    X_ = []
    scaler = MinMaxScaler()
    X_ = scaler.fit(X)
    X_ = scaler.transform(X)        
    return X_

##############
##############

###################
### CLASSIFIERS ###
###################

### RANDOM FOREST CLASSIFIER ###

def create_RF_kaggle_k(models = []):
    f = 3
    fibo = [1, 2, 3, 5, 8, 13, 21, 23, 34, 55, 89, 144, 233, 377, 610, 987] # 15 positions
    random_state = 9103
    #n_estimators = [1, 2, 3, 5, 8, 13, 21 ]#, 34, 55, 89, 144, 233]
    #n_estimators = fibo[f+f:f+f+f+f]
    n_estimators = [111, 639, 1316, 2021]
    min_samples_leaf = fibo[:f] #[fibo[0]] #fibo[:f*2]
    #k_features = fibo[f+f:f+f+2] # max 23
    criterion = ["gini"]
    k_features = [1111, 6666, 9999] # max 10000

    #scale = [False,True]

    m_id = 1
    for k in k_features:
        for n_e in n_estimators:
        #for m_depth in max_depth:
            #for s_s in min_samples_split:
                for s_l in min_samples_leaf:
                    for c in criterion:
                        #for k in k_features:
                            #for s in scale:
                                #models.append([m_id,n_e,s_l,k,s])
                                #models.append([m_id,k,n_e,s_l,s])
                                models.append([m_id,k,n_e,s_l,c])
                                m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET

def folds_fit_models_RF_kaggle_k(i):
    #path = up(__file__)
    file_name = path+'\\data\\folds\\amazon_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8')
    file_name = path+'\\data\\folds\\amazon_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8')
    X_train = df_train.drop(['Class','ID'], axis=1)
    y_train = df_train['Class']
    X_test = df_test.drop(['Class','ID'], axis=1)
    y_test = df_test['Class']

    results = pd.DataFrame()
    models = create_RF_kaggle_k([])
    #print(models)
    #k=9
    for m in models:
        start = time.time()
        result_row = {}
        #m_id,n_e,m_depth,s_s,s_l,k = m[:]
        #m_id,k,n_e,s_l,s = m[:]
        m_id,k,n_e,s_l,c = m[:]
        #print(m[:])
        result_row["fold"] = i
        result_row["model"] = str(m_id) + '_RFC'
        result_row["n_estimators"] = n_e
        #result_row["max_depth"] = m_depth
        #result_row["min_samples_split"] = s_s
        result_row["min_samples_leaf"] = s_l
        result_row["k_features"] = k
        #result_row["scale"] = s
        result_row["criterion"] = c


        #X_test = SelectKBest(chi2, k=k).fit_transform(X_test, y_test)
        selector = SelectKBest(f_classif, k=k)

        s = False

        if s:
            X_train_s = scale_minMax(X_train)
            X_test_s = scale_minMax(X_test)

            X_train_r = selector.fit_transform(X_train_s, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test_s[:,k_features]

            #print(X_train_s,X_train_s.shape)
            #print(X_test,X_test.shape)
            #break
        else:
            X_train_r = selector.fit_transform(X_train, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test.iloc[:,k_features]
            #print(X_train,X_train.shape)
            #print(X_test,X_test.shape)
            #break
        
        rfr = RandomForestClassifier(n_estimators=n_e, 
                                    #max_depth=m_depth, 
                                    random_state=random_state, 
                                    criterion=c, 
                                    #min_samples_split=s_s, 
                                    min_samples_leaf=s_l)
        rfr.fit(X_train_r, y_train)

        y_pred = rfr.predict(X_test_r)
        y_true = y_test

        result_row["score"] = round(rfr.score(X_test_r, y_test), 6)
        #result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)

        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)

        #file_name = path + '\\target\\amazon_results_partial_RF_model'+str(m)+'.csv' # name of the output file
        #write(results,file_name)

    return results

## AGGREGATE THE DATA INTO A TABLE

def aggregate_results_RF_kaggle_k(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    #results_folds['scale'] = results.groupby('model')['scale'].max()
    results_folds['n_estimators'] = results.groupby('model')['n_estimators'].max()
    #results_folds['max_depth'] = results.groupby('model')['max_depth'].max()
    #results_folds['min_samples_split'] = results.groupby('model')['min_samples_split'].max()
    results_folds['min_samples_leaf'] = results.groupby('model')['min_samples_leaf'].max()
    results_folds["k_features"] = results.groupby('model')['k_features'].max()
  
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    #results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    #results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()

    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds

def plot_RF(results_RF):
    r_columns = ['scale','n_estimators','min_samples_leaf','k_features']
    r_columns = ['n_estimators','min_samples_leaf','k_features']
    for c in r_columns:
        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_RF['score_mean'], x=results_RF[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )

        fileplotname = path+'\\plots\\boxplot_RF_'+str(c)+'.png'
        plt.savefig(fileplotname)
        plt.clf()
        #st.pyplot()
    return results_RF

def kaggle_result():

    kg_results = pd.DataFrame()
    
    #path = up(__file__)
    file_name = path+'\\data\\amazon_review_ID.shuf.lrn.csv'
    df_train = pd.read_csv(file_name, sep = ',', encoding='utf-8')
    file_name = path+'\\data\\amazon_review_ID.shuf.tes.csv'
    df_test = pd.read_csv(file_name, sep = ',', encoding='utf-8')

    X_train = df_train.drop(['Class','ID'], axis=1)
    y_train = df_train['Class']
    df_test_ID = df_test['ID']
    X_test = df_test.drop(['ID'], axis=1)



    #results = pd.DataFrame()
    #models = create_RF_kaggle_k([])

    selector = SelectKBest(f_classif, k=6369)
    s = False

    if s:
        X_train_s = scale_minMax(X_train)
        X_test_s = scale_minMax(X_test)

        X_train_r = selector.fit_transform(X_train_s, y_train)
        k_features = selector.get_support(indices=True)
        X_test_r = X_test_s[:,k_features]

    else:
        X_train_r = selector.fit_transform(X_train, y_train)
        k_features = selector.get_support(indices=True)
        X_test_r = X_test.iloc[:,k_features]
    
    rfr = RandomForestClassifier(n_estimators=1961, 
                                #max_depth=m_depth, 
                                random_state=random_state, 
                                #criterion="gini", 
                                criterion="entropy",
                                #min_samples_split=s_s, 
                                #min_samples_leaf=s_l)
                                )
    rfr.fit(X_train_r, y_train)



    predict_test = rfr.predict(X_test_r)

    #print (predict_test)

    kg_results['ID'] = df_test_ID

    kg_results["Class"] = predict_test

    time_f = int(round(time.time()-start_0,3))

    kg_results.to_csv(path + '\\target\\amazon_kaggle_n'+time_f+'.csv', sep=',', encoding='utf-8', index=False)

    return kg_results

### SUPPORT VECTOR MACHINE ###
## https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html

def create_SVM(models = []):
    f = 3
    fibo = [1, 2, 3, 5, 8, 13, 21, 23, 34, 55, 89, 144, 233, 377, 610, 987] # 15 positions

    #Specifies the kernel type to be used in the algorithm. 
    kernel = ['linear','poly','rbf','sigmoid']
    kernel = ['linear','poly']
    #Regularization parameter --> The strength of the regularization is inversely proportional to C. Must be strictly positive.
    C = fibo[:f+1]
    #Degree of the polynomial kernel function (‘poly’). Ignored by all other kernels.
    degree = fibo[1:f] #fibo[f+f:f+f+f+f]
    #gamma = ['scale','auto']
    #coef0 = [0,0.003,0.03,0.3,0.6,1,3,9]
    #data scaled or not scaled
    scale = [False,True]
    k_features = k_features = [1111, 6666, 9999]

    m_id = 1
    for ker in kernel:
        for c in C:
            for d in degree:
                #for g in gamma:
                    #for c0 in coef0:
                        for s in scale:
                            for k_f in k_features:
                                #models.append([m_id,ker,c,d,g,c0,s,k_f])
                                models.append([m_id,ker,c,d,s,k_f])
                                m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET

def folds_fit_models_SVM(i):
    #path = up(__file__)
    file_name = path+'\\data\\folds\\amazon_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8')
    file_name = path+'\\data\\folds\\amazon_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8')
    X_train = df_train.drop(['Class','ID'], axis=1)
    y_train = df_train['Class']
    X_test = df_test.drop(['Class','ID'], axis=1)
    y_test = df_test['Class']
    columns = X_train.columns

    results = pd.DataFrame()
    models = create_SVM([])
    #print(models)
    #k=9

    for m in models:
        start = time.time()
        result_row = {}
        #m_id,ker,c,d,g,c0,s,k_f = m[:]
        m_id,ker,c,d,s,k_f = m[:]

        if ker != 'poly':
            d=0

        #print(m[:])
        result_row["fold"] = i
        result_row["model"] = str(m_id) + '_SVM'
        result_row["kernel"] = ker
        result_row["C"] = c
        result_row["degree"] = d
        #result_row["gamma"] = g
        #result_row["coef0"] = c0
        result_row["scale"] = s
        result_row["k_features"] = k_f

        #X_test = SelectKBest(chi2, k=k).fit_transform(X_test, y_test)
        selector = SelectKBest(f_classif, k=k_f)

        if s:
            X_train_s = scale_minMax(X_train)
            X_test_s = scale_minMax(X_test)

            X_train_r = selector.fit_transform(X_train_s, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test_s[:,k_features]

        else:
            X_train_r = selector.fit_transform(X_train, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test.iloc[:,k_features]

        if ker != 'poly':
            #d=0
            svm = SVC(
                C=c, 
                kernel=ker, 
                #degree=d, 
                #gamma=g, 
                #coef0=c0,
                random_state=random_state
                )
        else:
            svm = SVC(
                C=c, 
                kernel=ker, 
                degree=d, 
                #gamma=g, 
                #coef0=c0,
                random_state=random_state
                )

        svm.fit(X_train_r, y_train)

        y_pred = svm.predict(X_test_r)
        y_true = y_test

        result_row["score"] = round(svm.score(X_test_r, y_test), 6)
        result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)

        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results


def aggregate_results_SVM(results):
    results_folds = pd.DataFrame()

    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['scale'] = results.groupby('model')['scale'].max()
    results_folds['kernel'] = results.groupby('model')['kernel'].max()
    results_folds['C'] = results.groupby('model')['C'].max()
    results_folds['degree'] = results.groupby('model')['degree'].max()
    #results_folds['gamma'] = results.groupby('model')['gamma'].max()
    #results_folds["coef0"] = results.groupby('model')['coef0'].max()
    results_folds["k_features"] = results.groupby('model')['k_features'].max()

    ## Evaluation Metrics
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()

    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds


def plot_SVM(results_SVM):
    r_columns = ['scale','kernel','C','degree','gamma','coef0','k_features']
    r_columns = ['scale','kernel','C','degree','k_features']
    for c in r_columns:
        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_SVM['score_mean'], x=results_SVM[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )
        fileplotname = path+'\\plots\\boxplot_SVM_'+str(c)+'.png'
        plt.savefig(fileplotname)
        plt.clf()
        #st.pyplot()
    return results_SVM



### RIDGE CLASSIFIER ####
### https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.RidgeClassifier.html

def create_Ridge(models = []):
    normalize = [False, True]
    #bias = [False, True]
    #interaction = [False, True]
    intercept = [True, False]
    #order = ['C','F']
    alpha = [0, 1e-15, 1e-10, 1e-5, 1, 5, 10, 15]
    #alpha = [0.000000001, 0.0001, 0.01, 0.1, 0.3, 0.5, 0.8, 1.0]
    #degree = [ 1, 2, 3 ]
    #scale = [False,True]
    ## TO TEST FASTER:
    #n_neighbors = [ 2, 4, 6]
    #algorithm = ["ball_tree", "kd_tree"]
    #weight = ["uniform"]

    # CREATE MODELS
    m_id = 1
    for n in normalize:
        for inter in intercept:
            for a in alpha:
                models.append([m_id,n,inter,a])
                m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models_Ridge(i):
    #path = up(__file__)
    file_name = path+'\\data\\folds\\amazon_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8')
    file_name = path+'\\data\\folds\\amazon_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8')
    X_train = df_train.drop(['Class','ID'], axis=1)
    y_train = df_train['Class']
    X_test = df_test.drop(['Class','ID'], axis=1)
    y_test = df_test['Class']
    columns = X_train.columns

    results = pd.DataFrame()
    models = create_Ridge([])

    for m in models:

        start = time.time()
        result_row = {}
        m_id,n,inter,a = m[:] 


        result_row["fold"] = i
        result_row["model"] = str(m_id) + '_Ridge'
        result_row["normalize"] = n
        result_row["intercept"] = inter
        result_row["alpha"] = a

        ridge = RidgeClassifier(alpha=a, fit_intercept=inter, normalize=n)
        ridge.fit(X_train, y_train)

        y_pred = ridge.predict(X_test)
        y_true = y_test

        result_row["score"] = round(ridge.score(X_test, y_test), 6)
        result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)


        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results


def aggregate_results_Ridge(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['normalize'] = results.groupby('model')['normalize'].max()
    results_folds['intercept'] = results.groupby('model')['intercept'].max()
    results_folds['alpha'] = results.groupby('model')['alpha'].max()

    ## Evaluation Metrics
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()


    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)

    return results_folds


def plot_Ridge(results_ridge):
    r_columns = ['normalize','alpha','intercept']
    for c in r_columns:

        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_ridge['score_mean'], x=results_ridge[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )
        fileplotname = path+'\\plots\\boxplot_Ridge_'+str(c)+'.png'
        plt.savefig(fileplotname)
        plt.clf()
        #st.pyplot()
    return results_ridge

#############
## HELPERS ##
#############

#dataframe reducer for trials
#reduces to percent % indicated
def df_reduce(df,percent):
    percent = float(percent)
    if percent == 1:
        return df
    ind = random.randint(0,len(df))
    index_r = random.sample(range(0,len(df)), int(len(df)*percent))
    df_r = df.iloc[index_r,:]
    return df_r


#scaler function
def scale_minMax(X):
    X_ = []
    scaler = MinMaxScaler()
    X_ = scaler.fit(X)
    X_ = scaler.transform(X)        
    return X_


def feature_list(x):
    print(list(x.columns))


#####################
### AMAZON SCRIPT ###
#####################


### FUNCTIONS ###

def amazon_folds(df):
    #df = pd.read_csv(path + '\data\amazon_1.csv', sep = ';', encoding='utf-8', dtype="str")
    cv = KFold(n_splits=10, random_state=9103, shuffle=True)
    #cv = GroupKFold(n_splits=10) # This is for not overlapping groups
    fold = 1

    for train_index, test_index in cv.split(df):
        df.iloc[train_index,:].to_csv(path + '\\data\\folds\\amazon_train_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        df.iloc[test_index,:].to_csv(path + '\\data\\folds\\amazon_test_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        fold = fold+1

    return df

def class_to_number(df_class):
    #u_class = df_class.unique()
    u_class = np.sort(df_class.unique())
    r_class = range(len(u_class))
    d_class = dict(zip(u_class, r_class))
    #print (d_class)
    return d_class

def number_to_class(df_class):
    #u_class = df_class.unique()
    u_class = np.sort(df_class.unique())
    r_class = range(len(u_class))
    d_class = dict(zip(r_class, u_class))
    #print (d_class)
    return d_class


####################################################
### HERE WE JUST SHOW THE IMPLEMENTATION VERBOSE ###
####################################################


print("# Machine Learning 2020S")
print("# Exercise 2 - Classification - Group 32")
print("## Amazon Reviews")
print('The data set looks like:')

#with st.echo():
# READ FROM DATA DIR
path = up(__file__)
#df_p = pd.read_csv(path + '\data\\amazon_review_ID.shuf.lrn.csv', sep = ',', encoding='utf-8')
amazon = pd.read_csv(path + '\data\\amazon_review_ID.shuf.lrn.csv', sep = ',', encoding='utf-8')
#df_p.to_numpy()
amazon.to_numpy()
print (amazon.head(6),amazon.shape)

print (amazon.columns,len(amazon.columns))
#print (amazon["Class"].unique(),amazon["Class"].nunique())
print (np.sort(amazon["Class"].unique()),amazon["Class"].nunique())
print (amazon.groupby('Class')['ID'].nunique())

classes_unique = amazon.groupby('Class')['ID'].nunique()
file_name = path + '\\target\\amazon_classes_unique.csv'
write(classes_unique,file_name)

desc = amazon.describe()
print(desc,desc.shape)
file_name = path + '\\target\\amazon_description.csv'
write(desc,file_name)

print ("Let's get started with the folds")
amazon_folds(amazon)

print ("Fitting the models:")


mit_allem = False
classifiers_models = ['RF','SVM','Ridge']
classifiers_models = ['Ridge']

for c_m in classifiers_models:
    results = pd.DataFrame()
    results_c = pd.DataFrame()
    print("## "+c_m+" ##")

    if c_m == 'RF':
        if mit_allem:
            arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_RF_kaggle_k)(i)  for i in range (1,11))
            for a in arr_results:
                results = results.append(a, ignore_index = True)
        else:
            for i in range (1,11):
                results = results.append(folds_fit_models_RF_kaggle_k(i), ignore_index = True)
                file_name = path + '\\target\\amazon_results_partial_thread_'+str(c_m)+'_'+str(i)+'.csv' # name of the output file
                write(results,file_name)

        print("## Results")
        print('Aggregated Table and Plots:')
        results_c = aggregate_results_RF_kaggle_k(results)
        print(results_c,results_c.shape)
        plot_RF(results_c)

        # EXPORT THE TABLE
        file_name = path + '\\target\\amazon_results_'+str(c_m)+'.csv' # name of the output file
        write(results_c,file_name)

    if c_m == 'SVM':
        if mit_allem:
            arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_SVM)(i)  for i in range (1,11))
            for a in arr_results:
                results = results.append(a, ignore_index = True)
        else:
            for i in range (1,11):
                results = results.append(folds_fit_models_SVM(i), ignore_index = True)
                file_name = path + '\\target\\amazon_results_partial_thread_'+str(c_m)+'_'+str(i)+'.csv' # name of the output file
                write(results,file_name)

        print("## Results")
        print('Aggregated Table and Plots:')
        results_c = aggregate_results_SVM(results)
        print(results_c,results_c.shape)
        plot_SVM(results_c)

        # EXPORT THE TABLE
        file_name = path + '\\target\\amazon_results_'+str(c_m)+'.csv' # name of the output file
        write(results_c,file_name)
    
    if c_m == 'Ridge':
        if mit_allem:
            arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_Ridge)(i)  for i in range (1,11))
            for a in arr_results:
                results = results.append(a, ignore_index = True)
        else:
            for i in range (1,11):
                results = results.append(folds_fit_models_Ridge(i), ignore_index = True)
                file_name = path + '\\target\\amazon_results_partial_thread_'+str(c_m)+'_'+str(i)+'.csv' # name of the output file
                write(results,file_name)

        print("## Results")
        print('Aggregated Table and Plots:')
        results_c = aggregate_results_Ridge(results)
        print(results_c,results_c.shape)
        plot_Ridge(results_c)

        # EXPORT THE TABLE
        file_name = path + '\\target\\amazon_results_'+str(c_m)+'.csv' # name of the output file
        write(results_c,file_name)

print (alles_good_papi())
print(round(time.time()-start_0,3))
exit()
#print(alles_good_papi())
#st.balloons()