import argparse
import pandas as pd
import numpy as np
import sys
import os
import os.path as path
from os.path import dirname as up
from unipath import Path
import random
import time

# Machinery for learning
import sklearn as sk
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif

# Let's get started

random_state = 9103
random.seed(random_state)
start_0 = time.time()


def alles_good_papi():
    return("Alles good papi")

def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())

def kaggle_result():
    kg_results = pd.DataFrame()

    file_name = path+'\\data\\amazon_review_ID.shuf.lrn.csv'
    df_train = pd.read_csv(file_name, sep = ',', encoding='utf-8')
    file_name = path+'\\data\\amazon_review_ID.shuf.tes.csv'
    df_test = pd.read_csv(file_name, sep = ',', encoding='utf-8')

    X_train = df_train.drop(['Class','ID'], axis=1)
    y_train = df_train['Class']
    df_test_ID = df_test['ID']
    X_test = df_test.drop(['ID'], axis=1)

    k = 6666
    n_e = 2121

    selector = SelectKBest(f_classif, k=k)
    X_train_r = selector.fit_transform(X_train, y_train)
    k_features = selector.get_support(indices=True)
    X_test_r = X_test.iloc[:,k_features]
    
    rfr = RandomForestClassifier(n_estimators=n_e, 
                                random_state=random_state)

    rfr.fit(X_train_r, y_train)
    predict_test = rfr.predict(X_test_r)

    kg_results['ID'] = df_test_ID
    kg_results["Class"] = predict_test
    print (kg_results.head(9))

    time_f = str(int(time.time()))

    kg_results.to_csv(path + '\\target\\amazon_kaggle_'+str(k)+'_'+str(n_e)+'_'+time_f+'.csv', sep=',', encoding='utf-8', index=False)
    return kg_results

####################################################
### HERE WE JUST SHOW THE CONSOLE ###
####################################################

print("# TU WIEN")
print("# Machine Learning 2020S")
print("# Exercise 2 - Classification - Group 32")
print("## Amazon Reviews - Kaggle")
print('The data set looks like:')

# READ FROM DATA DIR
path = up(__file__)
amazon = pd.read_csv(path + '\data\\amazon_review_ID.shuf.lrn.csv', sep = ',', encoding='utf-8')
amazon.to_numpy()
print (amazon.head(6),amazon.shape)

# Lets see what we have to predict..
#print (np.sort(amazon["Class"].unique()),amazon["Class"].nunique())
print (amazon.groupby('Class')['ID'].nunique(),amazon["Class"].nunique())

# Here we go..
kaggle_result()

print ("Kaggle results are cooked")
print (alles_good_papi())
print(round(time.time()-start_0,3))
exit()