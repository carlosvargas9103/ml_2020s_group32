#libraries
import streamlit as st
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import os.path as path
from os.path import dirname as up
from unipath import Path
import random
import seaborn as sns
import sklearn as sk
import plotly.express as px
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Lasso
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_classif
from sklearn.preprocessing import StandardScaler as st_scaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
from sklearn.svm import SVC
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
import time
from joblib import Parallel, delayed
import multiprocessing
from fancyimpute import KNN
from sklearn import preprocessing
from sklearn.metrics import roc_auc_score
from sklearn.linear_model import RidgeClassifier
from sklearn.metrics import log_loss

#timing of the algorithms
start_0 = time.time()
num_cores = multiprocessing.cpu_count()
random_state = 9103
random.seed(9103)




#####################
## BASIC FUNCTIONS ##
#####################

#function to show that everything works properly 
def alles_good_papi():
    return("Alles good papi")

#dataframe saver
def write(dataset, file_name):
    #set encoding for Spanish Excel settings
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())




###################
### CLASSIFIERS ###
###################

### RANDOM FOREST CLASSIFIER ###

def create_RF(models = []):
    f = 3
    fibo = [1, 2, 3, 5, 8, 13, 21, 23, 34, 55, 89, 144, 233, 377, 610, 987] # 15 positions
    random_state = 9103
    n_estimators = [1, 2, 3, 5, 8, 13, 21 ]#, 34, 55, 89, 144, 233]
    n_estimators = fibo[f+f:f+f+f+f]
    min_samples_leaf = fibo[:f+f] #[fibo[0]] #fibo[:f*2]
    #after trial and error
    k_features = [3,8,12,15]

    scale = [False,True]

    m_id = 1
    for n_e in n_estimators:
        #for m_depth in max_depth:
            #for s_s in min_samples_split:
                for s_l in min_samples_leaf:
                    for k in k_features:
                        for s in scale:
                            models.append([m_id,n_e,s_l,k,s])
                            m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET

def folds_fit_models_RF(i):
    #path = up(__file__)
    file_name = 'data\\folds\congress_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = 'data\\folds\congress_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['class'], axis=1)
    y_train = df_train['class']
    X_test = df_test.drop(['class'], axis=1)
    y_test = df_test['class']
    columns = X_train.columns

    results = pd.DataFrame()
    models = create_RF([])
    #st.write(models)
    #k=9
    for m in models:
        start = time.time()
        result_row = {}
        #m_id,n_e,m_depth,s_s,s_l,k = m[:]
        m_id,n_e,s_l,k,s = m[:]
        #st.write(m[:])
        result_row["fold"] = i
        result_row["model"] = str(m_id) + '_RFC'
        result_row["n_estimators"] = n_e
        #result_row["max_depth"] = m_depth
        #result_row["min_samples_split"] = s_s
        result_row["min_samples_leaf"] = s_l
        result_row["k_features"] = k
        result_row["scale"] = s

        #X_test = SelectKBest(chi2, k=k).fit_transform(X_test, y_test)
        selector = SelectKBest(f_classif, k=k)

        if s:
            X_train_s = scale_minMax(X_train)
            X_test_s = scale_minMax(X_test)

            X_train_r = selector.fit_transform(X_train_s, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test_s[:,k_features]

            #st.write(X_train_s,X_train_s.shape)
            #st.write(X_test,X_test.shape)
            #break
        else:
            X_train_r = selector.fit_transform(X_train, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test.iloc[:,k_features]
            #st.write(X_train,X_train.shape)
            #st.write(X_test,X_test.shape)
            #break
        
        rfr = RandomForestClassifier(n_estimators=n_e, 
                                    #max_depth=m_depth, 
                                    random_state=random_state, 
                                    criterion="gini", 
                                    #min_samples_split=s_s, 
                                    min_samples_leaf=s_l)
        rfr.fit(X_train_r, y_train)

        y_pred = rfr.predict(X_test_r)
        y_true = y_test
        

        result_row["score"] = round(rfr.score(X_test_r, y_test), 6)
        result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)

        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results

## AGGREGATE THE DATA INTO A TABLE

def aggregate_results_RF(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['scale'] = results.groupby('model')['scale'].max()
    results_folds['n_estimators'] = results.groupby('model')['n_estimators'].max()
    #results_folds['max_depth'] = results.groupby('model')['max_depth'].max()
    #results_folds['min_samples_split'] = results.groupby('model')['min_samples_split'].max()
    results_folds['min_samples_leaf'] = results.groupby('model')['min_samples_leaf'].max()
    results_folds["k_features"] = results.groupby('model')['k_features'].max()
  
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()

    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds

def plot_RF(results_RF):
    r_columns = ['scale','n_estimators','min_samples_leaf','k_features']
    for c in r_columns:
        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_RF['score_mean'], x=results_RF[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )
        st.pyplot()
    return results_RF

### SUPPORT VECTOR MACHINES ###
## https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html

def create_SVM(models = []):
    f = 3
    fibo = [1, 2, 3, 5, 8, 13, 21, 23, 34, 55, 89, 144, 233, 377, 610, 987] # 15 positions

    #Specifies the kernel type to be used in the algorithm. 
    kernel = ['linear','poly','rbf','sigmoid']
    kernel = ['linear','poly','rbf']
    #Regularization parameter --> The strength of the regularization is inversely proportional to C. Must be strictly positive.
    C = fibo[:f+1]
    #Degree of the polynomial kernel function (‘poly’). Ignored by all other kernels.
    degree = fibo[:f+1] #fibo[f+f:f+f+f+f]
    #gamma = ['scale','auto']
    #coef0 = [0,0.003,0.03,0.3,0.6,1,3,9]
    #data scaled or not scaled
    scale = [False,True]
    k_features = [5,10,15,16] 

    m_id = 1
    for ker in kernel:
        for c in C:
            for d in degree:
                #for g in gamma:
                    #for c0 in coef0:
                        for s in scale:
                            for k_f in k_features:
                                #models.append([m_id,ker,c,d,g,c0,s,k_f])
                                models.append([m_id,ker,c,d,s,k_f])
                                m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET

def folds_fit_models_SVM(i):
    #path = up(__file__)
    file_name = 'data\\folds\congress_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = 'data\\folds\congress_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['class'], axis=1)
    y_train = df_train['class']
    X_test = df_test.drop(['class'], axis=1)
    y_test = df_test['class']
    columns = X_train.columns

    results = pd.DataFrame()
    models = create_SVM([])
    #st.write(models)
    #k=9

    for m in models:
        start = time.time()
        result_row = {}
        #m_id,ker,c,d,g,c0,s,k_f = m[:]
        m_id,ker,c,d,s,k_f = m[:]

        if ker != 'poly':
            d=0

        #st.write(m[:])
        result_row["fold"] = i
        result_row["model"] = str(m_id) + '_SVM'
        result_row["kernel"] = ker
        result_row["C"] = c
        result_row["degree"] = d
        #result_row["gamma"] = g
        #result_row["coef0"] = c0
        result_row["scale"] = s
        result_row["k_features"] = k_f

        #X_test = SelectKBest(chi2, k=k).fit_transform(X_test, y_test)
        selector = SelectKBest(f_classif, k=k_f)

        if s:
            X_train_s = scale_minMax(X_train)
            X_test_s = scale_minMax(X_test)

            X_train_r = selector.fit_transform(X_train_s, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test_s[:,k_features]

            #st.write(X_train_s,X_train_s.shape)
            #st.write(X_test,X_test.shape)
            #break
        else:
            X_train_r = selector.fit_transform(X_train, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test.iloc[:,k_features]
            #st.write(X_train,X_train.shape)
            #st.write(X_test,X_test.shape)
            #break

        if ker != 'poly':
            #d=0
            svm = SVC(
                C=c, 
                kernel=ker, 
                #degree=d, 
                #gamma=g, 
                #coef0=c0,
                random_state=random_state,
                probability = True
                )
        else:
            svm = SVC(
                C=c, 
                kernel=ker, 
                degree=d, 
                #gamma=g, 
                #coef0=c0,
                random_state=random_state,
                probability = True
                )

        svm.fit(X_train_r, y_train)

        y_pred = svm.predict(X_test_r)
        y_true = y_test
        y_scores = svm.predict_proba(X_test_r)

        result_row["score"] = round(svm.score(X_test_r, y_test), 6)
        result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)
        #roc_auc_scores: Compute Area Under the Receiver Operating Characteristic Curve (ROC AUC) from prediction scores.
        result_row["roc_auc_score"] = round(roc_auc_score(y_true, y_scores[:,1]),6)
        #adding log-loss
        result_row["log_loss"] = round(log_loss(y_true, y_scores), 6)

        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)

    return results

## AGGREGATE THE DATA INTO A TABLE

def aggregate_results_SVM(results):
    results_folds = pd.DataFrame()

    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['scale'] = results.groupby('model')['scale'].max()
    results_folds['kernel'] = results.groupby('model')['kernel'].max()
    results_folds['C'] = results.groupby('model')['C'].max()
    results_folds['degree'] = results.groupby('model')['degree'].max()
    #results_folds['gamma'] = results.groupby('model')['gamma'].max()
    #results_folds["coef0"] = results.groupby('model')['coef0'].max()
    results_folds["k_features"] = results.groupby('model')['k_features'].max()

    ## Evaluation Metrics
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()
    #adding roc_auc_scores
    results_folds["roc_auc_score_mean"] = results.groupby('model')["roc_auc_score"].mean()
    results_folds["roc_auc_score:max"] = results.groupby('model')["roc_auc_score"].max()
    #adding log loss
    results_folds['log_loss_mean'] = results.groupby('model')['log_loss'].mean()
    results_folds['log_loss_min'] = results.groupby('model')['log_loss'].min()


    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)

    return results_folds


def plot_SVM(results_SVM):
    r_columns = ['scale','kernel','C','degree','gamma','coef0','k_features']
    r_columns = ['scale','kernel','C','degree','k_features']
    for c in r_columns:
        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_SVM['score_mean'], x=results_SVM[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )
        st.pyplot()
    return results_SVM





### RIDGE CLASSIFIER ####
### https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.RidgeClassifier.html

def create_Ridge(models = []):
    normalize = [False, True]
    #bias = [False, True]
    #interaction = [False, True]
    intercept = [True, False]
    #order = ['C','F']
    alpha = [0, 1e-15, 1e-10, 1e-5, 1, 5, 10, 15]
    #alpha = [0.000000001, 0.0001, 0.01, 0.1, 0.3, 0.5, 0.8, 1.0]
    #degree = [ 1, 2, 3 ]
    #scale = [False,True]
    ## TO TEST FASTER:
    #n_neighbors = [ 2, 4, 6]
    #algorithm = ["ball_tree", "kd_tree"]
    #weight = ["uniform"]

    # CREATE MODELS
    m_id = 1
    for n in normalize:
        for inter in intercept:
            for a in alpha:
                models.append([m_id,n,inter,a])
                m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models_Ridge(i):
    file_name = 'data\\folds\congress_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = 'data\\folds\congress_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['class'], axis=1)
    y_train = df_train['class']
    X_test = df_test.drop(['class'], axis=1)
    y_test = df_test['class']
    columns = X_train.columns

    results = pd.DataFrame()
    models = create_Ridge([])


    for m in models:

        start = time.time()
        result_row = {}
        m_id,n,inter,a = m[:] 


        result_row["fold"] = i
        result_row["model"] = str(m_id) + '_Ridge'
        result_row["normalize"] = n
        result_row["intercept"] = inter
        result_row["alpha"] = a

        ridge = RidgeClassifier(alpha=a, fit_intercept=inter, normalize=n)
        ridge.fit(X_train, y_train)

        y_pred = ridge.predict(X_test)
        y_true = y_test

        result_row["score"] = round(ridge.score(X_test, y_test), 6)
        result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)


        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results


def aggregate_results_Ridge(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['normalize'] = results.groupby('model')['normalize'].max()
    results_folds['intercept'] = results.groupby('model')['intercept'].max()
    results_folds['alpha'] = results.groupby('model')['alpha'].max()

    ## Evaluation Metrics
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()


    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)

    return results_folds


def plot_Ridge(results_ridge):
    r_columns = ['normalize','alpha','intercept']
    for c in r_columns:
        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_ridge['score_mean'], x=results_ridge[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )
        st.pyplot()
    return results_ridge


#############
## HELPERS ##
#############

#dataframe reducer for trials
#reduces to percent % indicated
def df_reduce(df,percent):
    percent = float(percent)
    if percent == 1:
        return df
    ind = random.randint(0,len(df))
    index_r = random.sample(range(0,len(df)), int(len(df)*percent))
    df_r = df.iloc[index_r,:]
    return df_r


#scaler function
def scale_minMax(X):
    X_ = []
    scaler = MinMaxScaler()
    X_ = scaler.fit(X)
    X_ = scaler.transform(X)        
    return X_


def feature_list(x):
    st.write(list(x.columns))





####################################################
### HERE WE JUST SHOW THE IMPLEMENTATION VERBOSE ###
####################################################


st.markdown("# Machine Learning 2020S")
st.markdown("# Exercise 2 - Classification - Group 32")


#description of the dataset
st.markdown("## Description of Congress Voting Dataset")
st.markdown('The data set looks like:')

with st.echo():
    # READ FROM DATA DIR
    df_p = pd.read_csv("data/CongressionalVotingID.shuf.train.csv", sep = ',', encoding='utf-8')
    congress = df_p


#print dataframe and shape
st.write(congress,congress.shape ) #no need to use describe since all the variables are categorical!

st.markdown("Show types: ")
st.write(congress.dtypes)


#Characterization (plotting)
st.markdown("## Characterizing of the datasets before preprocessing")
st.markdown('Here we generate some plots to understand better the data and the distribution of their features before preprocessing:')

#use the list to create features
#feature_list(df_p)

features = [
  "handicapped-infants",
  "water-project-cost-sharing",
  "adoption-of-the-budget-resolution",
  "physician-fee-freeze",
  "el-salvador-aid",
  "religious-groups-in-schools",
  "anti-satellite-test-ban",
  "aid-to-nicaraguan-contras",
  "mx-missile",
  "immigration",
  "synfuels-crporation-cutback",
  "education-spending",
  "superfund-right-to-sue",
  "crime",
  "duty-free-exports",
  "export-administration-act-south-africa"
]

#plotting
with st.echo():
    for i in features:
        ax = sns.countplot(x=df_p[i], hue="class", data=df_p)
        st.pyplot(clear_figure=True)
        #just in case we wanted to save them too:
        #plt.savefig(fname=f"plots\\before_prepro\countplot_{i}.png")
        

#preprocessing
st.markdown("## Preprocessing")


#inconsistent values
st.markdown("### Analysis of insconsistent values")
st.markdown("This dataframe does not present inconsistent values (i.e. outstanding outliers). Well, better said, it's very difficult to identify them since all data consists on yes/no entries. The only way to do that would be to go sample by sample and judging if something may be a mistaken entry. However, even though something may look weird, we could  not even prove visually that they could be mistaken entries. Thus, we keep all the samples.")
st.markdown("However, the ID column can be deleted since will not be used in the classification model: ")

with st.echo():
    #delete ID column
    df_p = df_p.drop(columns = ["ID"])
    st.write(df_p)



#feature encoding
st.markdown("### Encoding")
st.markdown("All the features are weighted categorical. They should be transformed to numerical. To do so, replace 'y' which means 'yes' by 1's and 'n' which means 'no' by 0's. One-hot-encoding is not required since weighted data is somehow already one-hot-encoded.")

with st.echo():
    #we could do a proper encoding using OriginalEncoder but knowing the structure of our data this saves time
    df_p = df_p.replace("y",1)
    df_p = df_p.replace("n",0)
    st.write(df_p, df_p.shape)

st.markdown("For the model, the labelled feature should be also encoded. For that purpose, the method LabelEncoder() from sklearn.preprocessing is used.")

with st.echo():
    le = preprocessing.LabelEncoder()
    #fit the encoder with the labelled (class) feautre
    le.fit(df_p["class"])
    #show classes to check no mistakes are made
    st.write("Clases:")
    st.write(list(le.classes_))
    #fit label encoder and return encoded labels
    #new encoding
    st.write("New Dataframe")
    df_p["class"] = le.transform(df_p["class"])
    st.write(df_p)



#missing values
st.markdown("### Imputation of missing values")
st.markdown("The dataframe contains around 200 samples. However, there are relatively a lot of missing values per column: ")

#show how many missing
with st.echo():
    #replace "unknown" by missing values
    df_p = df_p.replace("unknown", np.NaN)
    missing = df_p.isnull().sum()
    st.write(missing)
    #alternative df_p.info()
    missing.to_csv("missing.csv")

st.markdown("Deleting rows with missing values would reduce considerably the number of observations to train the model. Taking unknowns as cateogries for each feature would be too simple (specially for our case - weighted data). Thus, it's better to imputate those missing entries.")

#substitute missing using KNN
#why? widely use, performs good and multiple imputation being simpler is generally preferred
st.markdown("#### Method chosen: KNN")

with st.echo():
    #use KNN from fancyimpute
    #use 5 nearest neighbors to fill in each row's missing features
    imputer = KNN(k=5)
    #we round the numbers because KNN produces floats
    df_p.iloc[:,1:] = np.round(imputer.fit_transform(df_p.iloc[:,1:]))
    st.write(df_p)
    #df_p = KNN(k = 5).complete(df_p)
    #check that there are not missing anymore
    st.write("Missing after imputation: ")
    st.write(df_p.isnull().sum())



#scaling
st.markdown("### Scaling")
st.markdown("Scaling is highly recommended for classification methods. Specially for algorithms like SVM which are no scale invariant (which we will use). However, we have included scaling vs non-scaling when building the model so tha twe can compare the results. BUT, FOR THIS PARITUCLAR DATASET, WE WILL SEE THAT SCALLING LEADS TO SAME RESULTS AS NON-SCALING SINCE ALL THE FEATURES ARE weighted!!!")
#we could remove scaling for this models!!


#end of preprocessing
st.markdown("Preprocessing is done. Data is ready for the model. We can keep going with our analysis.")

with st.echo():
    #change name of the dataframe to df since prepro is done
    df = df_p



#Characterization (plotting)
st.markdown("## Characterizing of the datasets after preprocessing")
st.markdown("Here we generate some plots to understand better the data and the distribution of their features after preprocessing:")

with st.echo():
    for i in features:
        ax = sns.countplot(x=df[i], hue="class", data=df)
        st.pyplot(clear_figure=True)
        #just in case we wanted to save them too: 
        #plt.savefig(fname=f"plots\\before_prepro\countplot_{i}.png")



## Cross-Validation
st.markdown("## Split the dataset for 10-Folds Cross-Validation")
st.markdown('The resulting datasets are store in the directory /folds')

with st.echo():
    cv = KFold(n_splits=10, random_state=9103, shuffle=True)
    #cv = GroupKFold(n_splits=10) # This is for not overlapping groups
    fold = 1
    for train_index, test_index in cv.split(df_p):
        df.iloc[train_index,:].to_csv('data\\folds\congress_train_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        df.iloc[test_index,:].to_csv('data\\folds\congress_test_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        fold = fold+1

st.markdown('One dataset for training looks like:')
st.write(df.iloc[train_index,:],df.iloc[train_index,:].shape)
st.markdown('One dataset for testing looks like:')
st.write(df.iloc[test_index,:],df.iloc[test_index,:].shape)




##fit the models
st.markdown('## Fitting the models:')
st.markdown('Here we fit the models parallelizing the cross-validation')


with st.echo():

    mit_allem = True
    classifiers_models = ['RF','SVM','Ridge']


    for c_m in classifiers_models:
        results = pd.DataFrame()
        results_c = pd.DataFrame()
        st.markdown("## "+c_m)
        if c_m == 'RF':
            if mit_allem:
                arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_RF)(i)  for i in range (1,11))
                for a in arr_results:
                    results = results.append(a, ignore_index = True)
            else:
                for i in range (1,11):
                    results = results.append(folds_fit_models_RF(i), ignore_index = True)
            
            st.markdown("## Results")
            st.markdown('Aggregated Table and Plots:')
            results_c = aggregate_results_RF(results)
            st.write(results_c,results_c.shape)
            plot_RF(results_c)

        elif c_m == 'SVM':
            if mit_allem:
                arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_SVM)(i)  for i in range (1,11))
                for a in arr_results:
                    results = results.append(a, ignore_index = True)
            else:
                for i in range (1,11):
                    results = results.append(folds_fit_models_SVM(i), ignore_index = True)
            
            st.markdown("## Results")
            st.markdown('Aggregated Table and Plots:')
            results_c = aggregate_results_SVM(results)
            st.write(results_c,results_c.shape)
            plot_SVM(results_c)
        
        elif c_m == 'Ridge':
            if mit_allem:
                arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_Ridge)(i)  for i in range (1,11))
                for a in arr_results:
                    results = results.append(a, ignore_index = True)
            else:
                for i in range (1,11):
                    results = results.append(folds_fit_models_Ridge(i), ignore_index = True)
            
            st.markdown("## Results")
            st.markdown('Aggregated Table and Plots:')
            results_c = aggregate_results_Ridge(results)
            st.write(results_c,results_c.shape)
            plot_Ridge(results_c)

        # EXPORT THE TABLE
        file_name = 'target\congress_results_'+c_m+'.csv' # file name of the output file
        write(results_c,file_name)


st.success(round(time.time()-start_0,3))




#################
## CELEBRATION ##
#################

st.balloons()
st.write(alles_good_papi())