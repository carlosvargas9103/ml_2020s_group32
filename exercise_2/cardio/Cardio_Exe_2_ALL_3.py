import streamlit as st
import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import os.path as path
from os.path import dirname as up
from unipath import Path
import random
import seaborn as sns
import sklearn as sk
import plotly.express as px
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import Lasso
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.preprocessing import MinMaxScaler
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
from sklearn.feature_selection import f_classif
from sklearn.preprocessing import StandardScaler as st_scaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA

# Evaluation
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score


#from sklearn.metrics import confusion_matrix
import time
from joblib import Parallel, delayed
import multiprocessing


start_0 = time.time()
num_cores = multiprocessing.cpu_count()
random_state = 9103
random.seed(9103)



def alles_good_papi():
    return("Alles good papi")

def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())

def generate_missing(dataset):
    percentage = 0.2 # percentage of values to replace in one column or randomly across all attributes
    feature = None # column name to replace values only for a specific column e.g.: 'LB' or None to replaces values across all feature attributes
    random_seed = 9103 # initialization parameter of the random number generator
    random_seed_iteration = 1
    file_name = path + '\data\cgt_missing_'+percentage+'.csv' # file name of the output file
    
    random.seed(random_seed)
    #dataset = read()
    columne_names = list(dataset.columns.values)
    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV', 'MLTV', 'Width',
                'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean', 'Median', 'Variance', 'Tendency']
    number_of_features = len(features)
    number_of_rows = len(dataset.index)
    num_rxf = number_of_rows * number_of_features

    st.write("Number of columns: %d" % len(columne_names))
    st.write("Number of (real) features: %d" % number_of_features)
    st.write("Number of rows: %d" % number_of_rows)
    st.write("Number of feature values: %d" % num_rxf)

    if feature is None:
        number_of_missing_values = int(num_rxf * percentage)
        st.write("Number of feature values to replace with NaN: %d" % number_of_missing_values)
        for x in range(number_of_missing_values): 
            random_seed = random_seed + random_seed_iteration
            random.seed(random_seed)
            random_column = random.choice(features) # only replace values with NaN for real features
            dataset.loc[dataset.sample(n=1, random_state=random_seed).index, random_column] = np.NaN
        write(dataset, file_name)
    else:
        dataset_percentage = dataset.sample(frac=percentage, random_state=random_seed)
        data_rest = dataset.drop(dataset_percentage.index)
        number_of_missing_values_for_column = len(dataset_percentage.index)
        st.write("Number of feature value for column '%s' to replace with NaN: %d" % (feature, number_of_missing_values_for_column))
        dataset_percentage[feature] = np.NaN
        result = pd.concat([data_rest, dataset_percentage])
        result.sort_index(inplace=True) # sort by index
        write(result, file_name)
        
    return(alles_good_papi())

def model_fit_cv(model):
    result_row = {}
    m_id,n,b,interact,inter,d = model[:]
    result_row["model"] = m_id
    result_row["normalize"] = n
    result_row["bias"] = b
    result_row["interaction"] = interact
    result_row["intercept"] = inter
    result_row["degree"] = d

    start = time.time()

    poly = PolynomialFeatures(include_bias=b, interaction_only=interact, degree=d)
    lrm = LinearRegression(normalize=n, fit_intercept=inter)
    X_=poly.fit_transform(X)
    #X_t_train=poly.fit_transform(X_train)
    #X_t_test=poly.fit_transform(X_test)

    # CROSS-VALIDATION
    scores = []

    cv = KFold(n_splits=10, random_state=9103, shuffle=False)

    for train_index, test_index in cv.split(X_):
        X_train, X_test, y_train, y_test = X_[train_index], X_[test_index], y[train_index], y[test_index]
        lrm.fit(X_train, y_train)
        scores.append(lrm.score(X_test, y_test))
    
    score_mean = round(np.mean(scores),6)
    score_max = round(np.max(scores),6)

    # OR SIMPLY USING CROSS_VAL_SCORE
    #score_mean = round(np.mean(cross_val_score(lrm, X_, y, cv=cv)),6)
    #score_max = round(np.max(cross_val_score(lrm, X_, y, cv=cv)),6)
    
    time_train = round(time.time() - start,6)

    result_row["score_mean"] = score_mean
    result_row["score_max"] = score_max
    result_row["time"] = round(time.time() - start,6)

    #st.write(result_row)
    return result_row

def model_fit_folds(model):
    result_row = {}
    m_id,n,b,interact,inter,d = model[:]
    result_row["model"] = m_id
    result_row["normalize"] = n
    result_row["bias"] = b
    result_row["interaction"] = interact
    result_row["intercept"] = inter
    result_row["degree"] = d

    start = time.time()

    poly = PolynomialFeatures(include_bias=b, interaction_only=interact, degree=d)
    lrm = LinearRegression(normalize=n, fit_intercept=inter)

    X_=poly.fit_transform(X)

    # CROSS-VALIDATION
    scores = []

    cv = KFold(n_splits=10, random_state=9103, shuffle=False)

    for train_index, test_index in cv.split(X_):
        X_train, X_test, y_train, y_test = X_[train_index], X_[test_index], y[train_index], y[test_index]
        lrm.fit(X_train, y_train)
        scores.append(lrm.score(X_test, y_test))
    
    score_mean = round(np.mean(scores),6)
    score_max = round(np.max(scores),6)

    # OR SIMPLY USING CROSS_VAL_SCORE

    #score_mean = round(np.mean(cross_val_score(lrm, X_, y, cv=cv)),6)
    #score_max = round(np.max(cross_val_score(lrm, X_, y, cv=cv)),6)
    st.write(score_mean,score_max)

    end = time.time()
    time_train = round(end - start,6)

    result_row["score_mean"] = score_mean
    result_row["score_max"] = score_max
    result_row["time"] = time_train

    #st.write(result_row)

    return result_row

### LINEAR MODEL

def create_LM(models = []):
    normalize = [False, True]
    bias = [False, True]
    interaction = [False, True]
    intercept = [True, False]
    #order = ['C','F']
    degree = [ 1, 2, 3 ]
    #degree = [ 1]

    # CREATE MODELS
    m_id = 1
    for n in normalize:
        for b in bias:
            for interact in interaction:
                for inter in intercept:
                    for d in degree:
                        models.append([m_id,n,b,interact,inter,d])
                        m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models_LM(i):
    path = up(__file__)
    file_name = path+'\\data\folds\cardio_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = path+'\\data\folds\cardio_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['LB'], axis=1)
    y_train = y_train = df_train['LB']
    X_test = df_test.drop(['LB'], axis=1)
    y_test = df_test['LB']

    results = pd.DataFrame()
    models = create_LM([])    
    for m in models:
        start = time.time()
        result_row = {}
        m_id,n,b,interact,inter,d = m[:]
        result_row["fold"] = i
        result_row["model"] = m_id
        result_row["normalize"] = n
        result_row["bias"] = b
        result_row["interaction"] = interact
        result_row["intercept"] = inter
        result_row["degree"] = d
        poly = PolynomialFeatures(include_bias=b, interaction_only=interact, degree=d)
        lrm = LinearRegression(normalize=n, fit_intercept=inter)
        X_train_t = poly.fit_transform(X_train)
        X_test_t = poly.fit_transform(X_test)
        lrm.fit(X_train_t, y_train)
        result_row["score"] = round(lrm.score(X_test_t, y_test),6)
        result_row["time"] = round(time.time() - start,6)
        results = results.append(result_row, ignore_index = True)
    return results

def aggregate_results_LM(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['normalize'] = results.groupby('model')['normalize'].max()
    results_folds['bias'] = results.groupby('model')['bias'].max()
    results_folds['interaction'] = results.groupby('model')['interaction'].max()
    results_folds['intercept'] = results.groupby('model')['intercept'].max()
    results_folds['degree'] = results.groupby('model')['degree'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()
    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by='model_id', ascending=True, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds

### K-NEAREST NEIGHBORS

def create_KNN(models = []):
    n_neighbors = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 17, 23, 32 ]
    algorithm = ["auto", "ball_tree", "kd_tree", "brute"]
    weight = ["uniform", "distance"]
    p_metric = [1, 2]
    ## TO TEST FASTER:
    #n_neighbors = [ 2, 4, 6]
    #algorithm = ["ball_tree", "kd_tree"]
    #weight = ["uniform"]

    # CREATE MODELS
    m_id = 1
    for n in n_neighbors:
        for a in algorithm:
            for r in weight:
                for p in p_metric:
                    models.append([m_id,n,a,r,p])
                    m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models_KNN(i):
    path = up(__file__)
    file_name = path+'\\data\folds\cardio_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = path+'\\data\folds\cardio_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['LB'], axis=1)
    y_train = df_train['LB']
    X_test = df_test.drop(['LB'], axis=1)
    y_test = df_test['LB']

    results = pd.DataFrame()
    models = create_KNN([])
    #st.write(models)
    for m in models:
        start = time.time()
        result_row = {}
        m_id,n,a,r,p = m[:]
        result_row["fold"] = i
        result_row["model"] = m_id
        result_row["n_neighbors"] = n
        result_row["algorithm"] = a
        result_row["weight"] = r
        result_row["metric"] = p
        knnr = KNeighborsRegressor(n_neighbors = n, algorithm = a, weights = r, p = p)
        knnr.fit(X_train, y_train)
        #st.write(knnr)
        #st.write(X_test, y_test)
        #predicted = knnr.predict(X_test)
        result_row["score"] = round(knnr.score(X_test, y_test), 6)
        result_row["time"] = round(time.time() - start, 6)
        #st.write(knnr.score(X_test, y_test))
        results = results.append(result_row, ignore_index = True)
    return results

def aggregate_results_KNN(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['n_neighbors'] = results.groupby('model')['n_neighbors'].max()
    results_folds['algorithm'] = results.groupby('model')['algorithm'].max()
    results_folds['weight'] = results.groupby('model')['weight'].max()
    results_folds['metric'] = results.groupby('model')['metric'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()
    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['score_mean','score_max','time'], ascending=True, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds

### LASSO REGRESION

def create_Lasso(models = []):
    normalize = [False, True]
    #bias = [False, True]
    #interaction = [False, True]
    intercept = [True, False]
    #order = ['C','F']
    alpha = [0, 1e-15, 1e-10, 1e-5, 1, 5, 10, 15]
    #alpha = [0.000000001, 0.0001, 0.01, 0.1, 0.3, 0.5, 0.8, 1.0]
    alpha = [0, 1e-15, 1e-10, 1]
    #degree = [ 1, 2, 3 ]

    ## TO TEST FASTER:
    #n_neighbors = [ 2, 4, 6]
    #algorithm = ["ball_tree", "kd_tree"]
    #weight = ["uniform"]

    # CREATE MODELS
    m_id = 1
    for n in normalize:
        for inter in intercept:
            for a in alpha:
                models.append([m_id,n,inter,a])
                m_id = m_id+1
    return models

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models_Lasso(i):
    path = up(__file__)
    file_name = path+'\\data\folds\cardio_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = path+'\\data\folds\cardio_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['CLASS'], axis=1)
    y_train = df_train['CLASS']
    X_test = df_test.drop(['CLASS'], axis=1)
    y_test = df_test['CLASS']

    results = pd.DataFrame()
    models = create_Lasso([])
    #st.write(models)
    for m in models:
        start = time.time()
        result_row = {}
        m_id,n,inter,a = m[:] 
        result_row["fold"] = i
        result_row["model"] = m_id
        result_row["normalize"] = n
        result_row["intercept"] = inter
        result_row["alpha"] = a
        lasso = Lasso(alpha=a, fit_intercept=inter, normalize=n)
        lasso.fit(X_train, y_train)
        result_row["score"] = round(lasso.score(X_test, y_test), 6)
        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results

def aggregate_results_Lasso(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['normalize'] = results.groupby('model')['normalize'].max()
    results_folds['intercept'] = results.groupby('model')['intercept'].max()
    results_folds['alpha'] = results.groupby('model')['alpha'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()
    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['score_mean','score_max','time'], ascending=True, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds

### RANDOM FOREST

def create_RF_NOT(models = []):
    random_state = 9103
    n_estimators = [1,2,3,5,10,32,100]
    #n_estimators = [1, 8]
    max_depth = [6,18,32,None]
    #max_depth = [1, 2]
    min_samples_split = [2,6,18,32]
    min_samples_leaf = [1,3,6,18,32]
    ## TO TEST FASTER:
    n_estimators = [1,3,6,9,32]
    max_depth = [1,3,9,18,32,None]
    min_samples_split = [2,9,18,32]
    min_samples_leaf = [1,2,3,9,18]
    max_features = ['auto','sqrt','log2']
    # CREATE MODELS
    m_id = 1
    for n_e in n_estimators:
        for m_depth in max_depth:
            for s_s in min_samples_split:
                for s_l in min_samples_leaf:
                    models.append([m_id,n_e,m_depth,s_s,s_l])
                    m_id = m_id+1
    return models

### RANDOM FOREST parameters

def create_RF(models = []):
    f = 3
    fibo = [1, 2, 3, 5, 8, 13, 21, 23, 34, 55, 89, 144, 233, 377, 610, 987] # 15 positions
    random_state = 9103
    #n_estimators = [1,2,3,5,10,32,100]
    #n_estimators = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233]
    n_estimators = [1, 2, 3, 5, 8, 13, 21 ]#, 34, 55, 89, 144, 233]
    n_estimators = fibo[f+f:f+f+f+f]
    #max_depth = [6,18,32,None]
    #max_depth = [2,5,6,None]
    #max_depth = [fibo[-1]]#fibo[-f:] #[fibo[-1]]#999 #None #fibo[f:f*2] #, 13, 21 ]#, 34, 55, 89, 144, 233, None]
    #min_samples_split = [1, 2, 3, 5, 8, 13, 21 , 34, 55, 89, 144, 233, 1000]
    #min_samples_split = [fibo[1]] #[fibo[1]] #fibo[1:f+1]
    #min_samples_leaf = [1, 2, 3, 5, 8, 13, 21 , 34, 55, 89, 144, 233, 1000]
    min_samples_leaf = fibo[:f+f] #[fibo[0]] #fibo[:f*2]
    k_features = fibo[f+f:f+f+2] # max 26
    scale = [False,True]

    m_id = 1
    for n_e in n_estimators:
        #for m_depth in max_depth:
            #for s_s in min_samples_split:
                for s_l in min_samples_leaf:
                    for k in k_features:
                        for s in scale:
                            #models.append([m_id,n_e,m_depth,s_s,s_l,k])
                            models.append([m_id,n_e,s_l,k,s])
                            m_id = m_id+1
    return models

def scale_minMax(X):
    X_ = []
    scaler = MinMaxScaler()
    X_ = scaler.fit(X)
    X_ = scaler.transform(X)        
    return X_

### PARALLEL FUNCTION TO FIT THE MODELS WITH THE DATASET - ESTE ES EL BUENO ;)
def folds_fit_models_RF(i):
    #path = up(__file__)
    file_name = path+'\\data\\folds\cardio_train_'+str(i)+'.csv'
    df_train = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    file_name = path+'\\data\\folds\cardio_test_'+str(i)+'.csv'
    df_test = pd.read_csv(file_name, sep = ';', encoding='utf-8', dtype="float64")
    X_train = df_train.drop(['CLASS'], axis=1)
    y_train = df_train['CLASS']
    X_test = df_test.drop(['CLASS'], axis=1)
    y_test = df_test['CLASS']
    columns = X_train.columns

    results = pd.DataFrame()
    models = create_RF([])
    #st.write(models)
    #k=9
    for m in models:
        start = time.time()
        result_row = {}
        #m_id,n_e,m_depth,s_s,s_l,k = m[:]
        m_id,n_e,s_l,k,s = m[:]
        #st.write(m[:])
        result_row["fold"] = i
        result_row["model"] = m_id
        result_row["n_estimators"] = n_e
        #result_row["max_depth"] = m_depth
        #result_row["min_samples_split"] = s_s
        result_row["min_samples_leaf"] = s_l
        result_row["k_features"] = k
        result_row["scale"] = s

        #X_test = SelectKBest(chi2, k=k).fit_transform(X_test, y_test)
        selector = SelectKBest(f_classif, k=k)

        if s:
            X_train_s = scale_minMax(X_train)
            X_test_s = scale_minMax(X_test)

            X_train_r = selector.fit_transform(X_train_s, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test_s[:,k_features]

            #st.write(X_train_s,X_train_s.shape)
            #st.write(X_test,X_test.shape)
            #break
        else:
            X_train_r = selector.fit_transform(X_train, y_train)
            k_features = selector.get_support(indices=True)
            X_test_r = X_test.iloc[:,k_features]
            #st.write(X_train,X_train.shape)
            #st.write(X_test,X_test.shape)
            #break
        
        rfr = RandomForestClassifier(n_estimators=n_e, 
                                    #max_depth=m_depth, 
                                    random_state=random_state, 
                                    criterion="gini", 
                                    #min_samples_split=s_s, 
                                    min_samples_leaf=s_l)
        rfr.fit(X_train_r, y_train)

        y_pred = rfr.predict(X_test_r)
        y_true = y_test

        result_row["score"] = round(rfr.score(X_test_r, y_test), 6)
        result_row["accuracy"] = round(accuracy_score(y_true, y_pred), 6)
        result_row["recall"] = round(recall_score(y_true, y_pred, average='weighted'), 6)
        result_row["precision"] = round(precision_score(y_true, y_pred, average='weighted'), 6)
        result_row["f1_score"] = round(f1_score(y_true, y_pred, average='weighted'), 6)

        result_row["time"] = round(time.time() - start, 6)
        results = results.append(result_row, ignore_index = True)
    return results

def aggregate_results_RF(results):
    results_folds = pd.DataFrame()
    results_folds['model_id'] = results.groupby('model')['model'].max()
    results_folds['scale'] = results.groupby('model')['scale'].max()
    results_folds['n_estimators'] = results.groupby('model')['n_estimators'].max()
    #results_folds['max_depth'] = results.groupby('model')['max_depth'].max()
    #results_folds['min_samples_split'] = results.groupby('model')['min_samples_split'].max()
    results_folds['min_samples_leaf'] = results.groupby('model')['min_samples_leaf'].max()
    results_folds["k_features"] = results.groupby('model')['k_features'].max()
  
    results_folds['recall_mean'] = results.groupby('model')['recall'].mean()
    results_folds['recall_max'] = results.groupby('model')['recall'].max()
    results_folds['precision_mean'] = results.groupby('model')['precision'].mean()
    results_folds['precision_max'] = results.groupby('model')['precision'].max()
    results_folds['f1_score_mean'] = results.groupby('model')['f1_score'].mean()
    results_folds['f1_score_max'] = results.groupby('model')['f1_score'].max()
    results_folds['accuracy_mean'] = results.groupby('model')['accuracy'].mean()
    results_folds['accuracy_max'] = results.groupby('model')['accuracy'].max()
    results_folds['score_mean'] = results.groupby('model')['score'].mean()
    results_folds['score_max'] = results.groupby('model')['score'].max()

    results_folds['time'] = results.groupby('model')['time'].sum()
    results_folds.sort_values(by=['time'], ascending=True, inplace =True)
    results_folds.sort_values(by=['score_mean','f1_score_mean','score_max','f1_score_max'], ascending=False, inplace =True)
    results_folds.reset_index(drop=True, inplace=True)
    #results_folds.columns = ["Party","Bundesland","Votes (%)"] # order of columns
    return results_folds


def df_reduce(df,percent):
    percent = float(percent)
    if percent == 1:
        return df
    ind = random.randint(0,len(df))
    index_r = random.sample(range(0,len(df)), int(len(df)*percent))
    df_r = df.iloc[index_r,:]
    return df_r


### HERE WE JUST SHOW THE IMPLEMENTATION

st.markdown("# Machine Learning 2020S")
st.markdown("# Exercise 2 - Classification - Group 32")
st.markdown("## Cardiotocography")
st.markdown('The data set looks like:')

with st.echo():
    # READ FROM DATA DIR
    path = up(__file__)
    #st.write(path)
    #path_2 = path
    df_p = pd.read_csv(path + '\data\cardio_0.csv', sep = ';', encoding='utf-8')
    cardio = df_p
    cardio = df_reduce(df_p,0.13)
    #cardio = df_reduce(df_p,1)

desc = cardio.describe()
st.write(cardio,cardio.shape)
st.write(desc,desc.shape)


st.markdown("## Some preprocessing is missing")
st.markdown('Here:')

with st.echo():
    # HERE THE PREPROCESSING IS MISSING
    st.write("Yes, some preprocessing (z.B. imputation of missing values and so on..)")

st.markdown("## Characterizing of the datasets after preprocessing")
st.markdown('Here we generate some plots to understand better the data and the distribution of their features:')

def plot_data_charateristics(df,features,x):

    #x='CLASS'
    #features = ['CLASS']
    #df = df
    for f in features:
        #fig = px.box(cardio, x='CLASS', y=f, color='CLASS' , notched=True, title=f+' distribution')
        #st.plotly_chart(fig)
        bplot=sns.boxplot(y=df[f], x=df[x], 
                        #data=cardio_plot,
                        width=0.5,
                        #palette="colorblind")
                        palette="bright")
        
        #bplot.set_xticklabels(bplot.get_xticklabels(), rotation=-45)

        
        # add swarmplot
        #bplot=sns.swarmplot(y=cardio[f], x=cardio['CLASS'], 
                        #data=cardio_plot,
                        #color=".2",
                        #alpha=0.75)

        st.pyplot()

    return alles_good_papi()

with st.echo():
    
    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV',
            'MLTV', 'Width', 'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean',
            'Median', 'Variance', 'Tendency', 'SUSP', 'CLASS', 'NSP']
    features = ['CLASS']
    plot_data_charateristics(df=cardio,features=features,x='CLASS')
    

st.markdown("## Preprocessing the dataset for the Regression model")
st.markdown('First selecting some of the columns')

with st.echo():
    features = ['LB', 'AC', 'FM', 'UC', 'DL', 'DS', 'DP', 'ASTV', 'MSTV', 'ALTV',
                'MLTV', 'Width', 'Min', 'Max', 'Nmax', 'Nzeros', 'Mode', 'Mean',
                'Median', 'Variance', 'Tendency', 'SUSP', 'CLASS', 'NSP']
    df = cardio.loc[:,features]
    write(df,path + '\data\cardio_1.csv')
    st.write(df,df.shape)

st.markdown("## Split the dataset for 10-Folds Cross-Validation")
st.markdown('The resulting datasets are store in the directory /folds')

with st.echo():
    #current = Path(os.getcwd())
    df = pd.read_csv(path + '\data\cardio_1.csv', sep = ';', encoding='utf-8', dtype="str")
    cv = KFold(n_splits=10, random_state=9103, shuffle=True)
    #cv = GroupKFold(n_splits=10) # This is for not overlapping groups
    fold = 1
    for train_index, test_index in cv.split(df):
        df.iloc[train_index,:].to_csv(path + '\\data\\folds\cardio_train_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        df.iloc[test_index,:].to_csv(path + '\\data\\folds\cardio_test_'+str(fold)+'.csv', sep=';', encoding='utf-8', index=False)
        fold = fold+1

st.markdown('One dataset for training looks like:')
st.write(df.iloc[train_index,:],df.iloc[train_index,:].shape)
st.markdown('One dataset for testing looks like:')
st.write(df.iloc[test_index,:],df.iloc[test_index,:].shape)

st.markdown('## Fitting the models!')
st.markdown('Here we fit the models parallelizing the cross-validation')

with st.echo():
    results = pd.DataFrame()

    mit_alles = True

    if mit_alles:
        arr_results = Parallel(n_jobs=num_cores)(delayed(folds_fit_models_RF)(i)  for i in range (1,11))
        for a in arr_results:
            results = results.append(a, ignore_index = True)
    else:
        for i in range (1,11):
            results = results.append(folds_fit_models_RF(i), ignore_index = True)
    
    results_RF = aggregate_results_RF(results)    

    # EXPORT THE FILE
    #current = up(__file__)
    file_name = path + '\\target\cardio_results_RF.csv' # file name of the output file
    write(results_RF,file_name)

def plot_RF(results_RF):
    r_columns = ['scale','n_estimators','min_samples_leaf','k_features']
    for c in r_columns:
        #fig = px.box(results, x=c, y="score_mean", color=c , notched=True, title=c+' vs. score_mean')
        #st.plotly_chart(fig)

        bplot=sns.boxplot(y=results_RF['score_mean'], x=results_RF[c], showfliers=False,
                #data=diamonds_clean_plot,
                width=0.3,
                #palette="colorblind")
                #palette="bright"
                )
        st.pyplot()
    return results_RF


st.markdown("## Let's analyse the results")
st.markdown('Here we generate an aggregated table with the results and some plots that help us to understand how does the models fit on the data:')
with st.echo():
    st.write(results_RF,results_RF.shape)
    plot_RF(results_RF)




st.success(round(time.time()-start_0,3))
#st.write(alles_good_papi())
st.balloons()